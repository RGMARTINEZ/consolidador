package model.database.sqlite;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class TableCreator {

    public static void main(String[] args) {
        crearIngresos();
        crearFotocopias();
        crearAnulados();
        crearOtrosConceptos();
        crearConsolidadoIncremento();
        crearExentos();
        crearIngresosFP();
        crearIngresosTR();
        crearResumenConsolidado();


    }

    private static final Logger LOGGER = LoggerFactory.getLogger(TableCreator.class);

    public static void crearIngresos() {
        String sql = "CREATE TABLE IF NOT EXISTS con_ingresos (\n"
                + "	ing_id integer PRIMARY KEY,\n"
                + "	ing_oficina text NOT NULL,\n"
                + "	ing_nombre_oficina text NOT NULL,\n"
                + "	ing_periodo text NOT NULL,\n"
                + "	ing_fecha_arrastre date NOT NULL,\n"
                + "	ing_fecha_turno text,\n"
                + "	ing_turno text NOT NULL,\n"
                + "	ing_liquidador text NOT NULL,\n"
                + "	ing_concepto text NOT NULL,\n"
                + "	ing_valor_base number,\n"
                + "	ing_valor_derechos number,\n"
                + "	ing_valor_incremento number,\n"
                + "	ing_canal_pago text,\n"
                + "	ing_fecha_pago text,\n"
                + "	ing_forma_pago text,\n"
                + "	ing_cheque_consignacion text,\n"
                + "	ing_pin_doc_pago text,\n"
                + "	ing_fecha_sistema text,\n"
                + "	capacity real\n"
                + ");";
        try (Connection conn = DriverManager.getConnection(DatabaseCreator.URL);
             Statement stmt = conn.createStatement()) {
            // create a new table
            stmt.execute(sql);
            LOGGER.info("Tabla de ingresos creada SQLite ...");
        } catch (SQLException e) {
            LOGGER.warn("Error al crear la tabla SQLite", e);
        }
    }
    
    public static void crearFotocopias() {
        String sql = "CREATE TABLE IF NOT EXISTS con_fotocopias (\n"
                + "	ing_id integer PRIMARY KEY,\n"
                + "	ing_oficina text NOT NULL,\n"
                + "	ing_nombre_oficina text NOT NULL,\n"
                + "	ing_periodo text NOT NULL,\n"
                + "	ing_fecha_arrastre date NOT NULL,\n"
                + "	ing_fecha_turno text,\n"
                + "	ing_turno text NOT NULL,\n"
                + "	ing_liquidador text NOT NULL,\n"
                + "	ing_concepto text NOT NULL,\n"
                + "	ing_valor_base number,\n"
                + "	ing_valor_derechos number,\n"
                + "	ing_valor_incremento number,\n"
                + "	ing_canal_pago text,\n"
                + "	ing_fecha_pago text,\n"
                + "	ing_forma_pago text,\n"
                + "	ing_cheque_consignacion text,\n"
                + "	ing_pin_doc_pago text,\n"
                + "	ing_fecha_sistema text,\n"
                + "	capacity real\n"
                + ");";
        try (Connection conn = DriverManager.getConnection(DatabaseCreator.URL);
             Statement stmt = conn.createStatement()) {
            // create a new table
            stmt.execute(sql);
            LOGGER.info("Tabla de fotocopias creada SQLite ...");
        } catch (SQLException e) {
            LOGGER.warn("Error al crear la tabla SQLite", e);
        }
    }
    
    public static void crearAnulados() {
        String sql = "CREATE TABLE IF NOT EXISTS con_anulados (\n"
                + "	ing_id integer PRIMARY KEY,\n"
                + "	ing_oficina text NOT NULL,\n"
                + "	ing_nombre_oficina text NOT NULL,\n"
                + "	ing_periodo text NOT NULL,\n"
                + "	ing_fecha_arrastre date NOT NULL,\n"
                + "	ing_fecha_turno text,\n"
                + "	ing_turno text NOT NULL,\n"
                + "	ing_liquidador text NOT NULL,\n"
                + "	ing_concepto text NOT NULL,\n"
                + "	ing_valor_base number,\n"
                + "	ing_valor_derechos number,\n"
                + "	ing_valor_incremento number,\n"
                + "	ing_canal_pago text,\n"
                + "	ing_fecha_pago text,\n"
                + "	ing_forma_pago text,\n"
                + "	ing_cheque_consignacion text,\n"
                + "	ing_pin_doc_pago text,\n"
                + "	ing_fecha_sistema text,\n"
                + "	capacity real\n"
                + ");";
        try (Connection conn = DriverManager.getConnection(DatabaseCreator.URL);
             Statement stmt = conn.createStatement()) {
            // create a new table
            stmt.execute(sql);
            LOGGER.info("Tabla de anulados creada SQLite ...");
        } catch (SQLException e) {
            LOGGER.warn("Error al crear la tabla SQLite", e);
        }
    }
    
    public static void crearOtrosConceptos() {
        String sql = "CREATE TABLE IF NOT EXISTS con_otros_conceptos (\n"
                + "	ing_id integer PRIMARY KEY,\n"
                + "	ing_oficina text NOT NULL,\n"
                + "	ing_nombre_oficina text NOT NULL,\n"
                + "	ing_periodo text NOT NULL,\n"
                + "	ing_fecha_arrastre date NOT NULL,\n"
                + "	ing_fecha_turno text,\n"
                + "	ing_turno text NOT NULL,\n"
                + "	ing_liquidador text NOT NULL,\n"
                + "	ing_concepto text NOT NULL,\n"
                + "	ing_valor_base number,\n"
                + "	ing_valor_derechos number,\n"
                + "	ing_valor_incremento number,\n"
                + "	ing_canal_pago text,\n"
                + "	ing_fecha_pago text,\n"
                + "	ing_forma_pago text,\n"
                + "	ing_cheque_consignacion text,\n"
                + "	ing_pin_doc_pago text,\n"
                + "	ing_fecha_sistema text,\n"
                + "	capacity real\n"
                + ");";
        try (Connection conn = DriverManager.getConnection(DatabaseCreator.URL);
             Statement stmt = conn.createStatement()) {
            // create a new table
            stmt.execute(sql);
            LOGGER.info("Tabla de otros conceptos creada SQLite ...");
        } catch (SQLException e) {
            LOGGER.warn("Error al crear la tabla SQLite", e);
        }
    }
    
    public static void crearConsolidadoIncremento() {
        String sql = "CREATE TABLE IF NOT EXISTS con_consolidado_incremento (\n"
                + "	ing_id integer PRIMARY KEY,\n"
                + "	ing_oficina text NOT NULL,\n"
                + "	ing_nombre_oficina text NOT NULL,\n"
                + "	ing_periodo text NOT NULL,\n"
                + "	ing_fecha_arrastre date NOT NULL,\n"
                + "	ing_circulo_orip text,\n"
                + "	ing_fecha_turno text,\n"
                + "	ing_valor_liquidacion number,\n"
                + "	ing_valor_recaudo_inc number,\n"
                + "	ing_canal_pago text,\n"
                + "	ing_forma_pago text,\n"
                + "	ing_fecha_sistema text,\n"
                + "	capacity real\n"
                + ");";
        try (Connection conn = DriverManager.getConnection(DatabaseCreator.URL);
             Statement stmt = conn.createStatement()) {
            // create a new table
            stmt.execute(sql);
            LOGGER.info("Tabla de consolidado incremento creada SQLite ...");
        } catch (SQLException e) {
            LOGGER.warn("Error al crear la tabla SQLite", e);
        }
    }
    
    
    public static void crearExentos() {
        String sql = "CREATE TABLE IF NOT EXISTS con_exentos (\n"
                + "	ing_id integer PRIMARY KEY,\n"
                + "	ing_oficina text NOT NULL,\n"
                + "	ing_nombre_oficina text NOT NULL,\n"
                + "	ing_periodo text NOT NULL,\n"
                + "	ing_fecha_arrastre date NOT NULL,\n"
                + "	ing_fecha_turno text,\n"
                + "	ing_turno text NOT NULL,\n"
                + "	ing_liquidador text NOT NULL,\n"
                + "	ing_concepto text NOT NULL,\n"
                + "	ing_tipo_liq text,\n"
                + "	ing_valor_base number,\n"
                + "	ing_valor_derechos number,\n"
                + "	ing_valor_incremento number,\n"
                + "	ing_canal_pago text,\n"
                + "	ing_fecha_pago text,\n"
                + "	ing_forma_pago text,\n"
                + "	ing_cheque_consignacion text,\n"
                + "	ing_pin_doc_pago text,\n"
                + "	ing_fecha_sistema text,\n"
                + "	capacity real\n"
                + ");";
        try (Connection conn = DriverManager.getConnection(DatabaseCreator.URL);
             Statement stmt = conn.createStatement()) {
            // create a new table
            stmt.execute(sql);
            LOGGER.info("Tabla de exentos creada SQLite ...");
        } catch (SQLException e) {
            LOGGER.warn("Error al crear la tabla SQLite", e);
        }
    }
    
    
    public static void crearIngresosFP() {
        String sql = "CREATE TABLE IF NOT EXISTS con_ingresos_fp (\n"
                + "	ing_id integer PRIMARY KEY,\n"
                + "	ing_oficina text NOT NULL,\n"
                + "	ing_nombre_oficina text NOT NULL,\n"
                + "	ing_periodo text NOT NULL,\n"
                + "	ing_fecha_arrastre date NOT NULL,\n"
                + "	ing_fecha_turno text,\n"
                + "	ing_turno text NOT NULL,\n"
                + "	ing_liquidador text NOT NULL,\n"
                + "	ing_concepto text NOT NULL,\n"
                + "	ing_valor_base number,\n"
                + "	ing_valor_derechos number,\n"
                + "	ing_valor_incremento number,\n"
                + "	ing_canal_pago text,\n"
                + "	ing_fecha_pago text,\n"
                + "	ing_forma_pago text,\n"
                + "	ing_cheque_consignacion text,\n"
                + "	ing_pin_doc_pago text,\n"
                + "	ing_fecha_sistema text,\n"
                + "	capacity real\n"
                + ");";
        try (Connection conn = DriverManager.getConnection(DatabaseCreator.URL);
             Statement stmt = conn.createStatement()) {
            // create a new table
            stmt.execute(sql);
            LOGGER.info("Tabla de ingresos fp creada SQLite ...");
        } catch (SQLException e) {
            LOGGER.warn("Error al crear la tabla SQLite", e);
        }
    }
    
    
    public static void crearIngresosTR() {
        String sql = "CREATE TABLE IF NOT EXISTS con_ingresos_tr (\n"
                + "	ing_id integer PRIMARY KEY,\n"
                + "	ing_oficina text NOT NULL,\n"
                + "	ing_nombre_oficina text NOT NULL,\n"
                + "	ing_periodo text NOT NULL,\n"
                + "	ing_fecha_arrastre date NOT NULL,\n"
                + "	ing_fecha_turno text,\n"
                + "	ing_turno text NOT NULL,\n"
                + "	ing_liquidador text NOT NULL,\n"
                + "	ing_concepto text NOT NULL,\n"
                + "	ing_valor_base number,\n"
                + "	ing_valor_derechos number,\n"
                + "	ing_valor_incremento number,\n"
                + "	ing_canal_pago text,\n"
                + "	ing_fecha_pago text,\n"
                + "	ing_forma_pago text,\n"
                + "	ing_cheque_consignacion text,\n"
                + "	ing_pin_doc_pago text,\n"
                + "	ing_fecha_sistema text,\n"
                + "	capacity real\n"
                + ");";
        try (Connection conn = DriverManager.getConnection(DatabaseCreator.URL);
             Statement stmt = conn.createStatement()) {
            // create a new table
            stmt.execute(sql);
            LOGGER.info("Tabla de ingresos tr creada SQLite ...");
        } catch (SQLException e) {
            LOGGER.warn("Error al crear la tabla SQLite", e);
        }
    }
    
    

    
    public static void crearResumenConsolidado() {
        String sql = "CREATE TABLE IF NOT EXISTS con_resumen_consolidado (\n"
                + "	ing_id integer PRIMARY KEY,\n"
                + "	ing_oficina text NOT NULL,\n"
                + "	ing_nombre_oficina text NOT NULL,\n"
                + "	ing_periodo text NOT NULL,\n"
                + "	ing_fecha_arrastre date NOT NULL,\n"
                + "	ing_oficina_resumen text,\n"
                + "	ing_fec_ini text,\n"
                + "	ing_fec_fin text,\n"
                + "	ing_tabla_base text,\n"
                + "	ing_cant_certif number,\n"
                + "	ing_valor_certif number,\n"
                + "	ing_cant_fotoc number,\n"
                + "	ing_valor_fotoc number,\n"
                
                + "	ing_cant_doctos number,\n"
                + "	ing_valor_doctos number,\n"
                + "	ing_cant_mayvr number,\n"
                + "	ing_valor_mayvr number,\n"
                + "	ing_cant_mayvrc number,\n"
                + "	ing_valor_mayvrc number,\n"
                + "	ing_cant_otrre number,\n"
                + "	ing_valor_otrre number,\n"
                + "	ing_cant_docto_anual number,\n"
                + "	ing_valor_docto_anual number,\n"
                + "	ing_cant_certi_anual number,\n"
                + "	ing_valor_certi_anual number,\n"
                
                + "	ing_valor_wcantinccdd number,\n"
                + "	ing_valor_wvlrinccdd number,\n"
                + "	ing_valor_wcantinccdmp number,\n"
                + "	ing_valor_wvlrinccdmp number,\n"
                
                + "	ing_fecha_sistema text\n"
                + ");";
        try (Connection conn = DriverManager.getConnection(DatabaseCreator.URL);
             Statement stmt = conn.createStatement()) {
            // create a new table
            stmt.execute(sql);
            LOGGER.info("Tabla de exentos resumen consolidado SQLite ...");
        } catch (SQLException e) {
            LOGGER.warn("Error al crear la tabla SQLite", e);
        }
    }

}
