package model.database.oracle;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 * Clase encargada de la conexión a la base de datos de Folio
 *
 * @author Fabian Agudelo
 */
public class GenericConnection {

    private static Connection ora8conn = null;
    private static final Logger LOGGER = LoggerFactory.getLogger(QueryManagerFolio.class);

    /**
     * Crea una conexión a la base de datos de Folio de la URL enviada.
     *
     * @param urlConn Conexión con el puerto y SID
     * @param user    Usuario de conexión a la DB
     * @param pass    Clave de acceso a la DB
     * @return Dto de tipo Conexion con todos los datos asociados
     * @throws Exception Error al acceder al SQLite
     */
    public static Connection crearConexion(String urlConn, String user, String pass) throws Exception {
        //Forma de conexión mediante driver instalado manualmente en las dependiencias
        Properties props = new Properties();
        props.setProperty("user", user);
        props.setProperty("password", pass);
        try {
            Class.forName("oracle.jdbc.OracleDriver");
            ora8conn = DriverManager.getConnection(urlConn, props);
            LOGGER.info( "CONEXION",urlConn );

        } catch (Exception e) {
            System.out.println("[URL:" + urlConn + "] Conexión no obtenida..." );

            e.printStackTrace();
        }
        return ora8conn;
    }

}
