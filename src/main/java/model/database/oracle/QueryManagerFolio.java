package model.database.oracle;

import model.database.oracle.transformer.ResultSetTransformer;
import model.database.oracle.transformer.ResultSetTransformerConsoIncremento;


import model.dto.IngresoDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Locale;

/**
 * Clase encargada de todas las consultas a la base de datos de Folio
 *
 * @author Fabian Agudelo
 */
public class QueryManagerFolio {
    private static final Logger LOGGER = LoggerFactory.getLogger(QueryManagerFolio.class);
    
    public List<IngresoDto> obtenerFotocopias(String url, String user, String pass, LocalDate fecha) throws Exception {
        List<IngresoDto> lista;
        
        LOGGER.info("Obteniendo información de fotocopias periodo");
        Connection conn = GenericConnection.crearConexion(url, user, pass);

        String sql ="select  to_char(fecsis,'dd-mm-yyyy') as FEC_RADICA,\n" +
                "       pag.nroradica as NRORADICA,\n" +
                "       usuario as LIQUIDADOR,\n" +
                "       decode(pag.concepto,'P','FOTOC.','OTROS') as CONCEPTO,\n" +
                "       pag.valor as VALOR_PAGO, '' AS VALOR_DERECHOS, '' AS VLR_INCREMENTO,\n" +
                "       to_char(fecha,'dd-mm-yyyy') FEC_PAGO,\n" +
                "       substr(pag.FORMA||'-'||tip.descripcion,1,40) as FORMA_PAGO, substr(pag.CANAL||'-'||can.descripcion,1,40) as CANAL_PAGO,  '' AS FEC_SISTEMA,\n" +
                "       pag.CHEQUE as CHEQUE_CONSIG,\n" +
                "       to_char(pag.APROBACION) as PIN_DOC_PAGO\n" +
                "from pagos pag, canales_pago can,\n" +
                "     tipo_pago tip\n" +
                "where concepto in ('P')\n" +
                "  and to_char(pag.fecsis, 'mm-yyyy') = '%MONTH%-%YEAR%'\n" +
                "  and pag.canal=can.id_canal and pag.forma=tip.id_tipo_pago\n" +
                "group by pag.nroradica, to_char(pag.fecsis,'dd-mm-yyyy'), pag.usuario, pag.concepto,pag.valor, substr(pag.CANAL||'-'||can.descripcion,1,40),\n" +
                "substr(pag.FORMA||'-'||tip.descripcion,1,40), to_char(fecha,'dd-mm-yyyy'), pag.cheque, pag.aprobacion";
        
        sql = sql.replace("%MONTH%", DateTimeFormatter.ofPattern("MM").format(fecha));
        sql = sql.replace("%YEAR%", String.valueOf(fecha.getYear()));
        LOGGER.info("sql = " + sql);
        try (PreparedStatement ps = conn.prepareStatement(sql)) {
            ResultSet rs = ps.executeQuery();
            lista = ResultSetTransformer.transformarFotocopias(rs);
            LOGGER.info( "RESULTADO", lista );

            rs.close();
            conn.close();
        }
        return lista;
    }
    
    
    
    public List<IngresoDto> obtenerAnuladosPeriodo(String url, String user, String pass, LocalDate fecha) throws Exception {
        List<IngresoDto> lista;
        
        LOGGER.info("Obteniendo información de anulados periodo");
        Connection conn = GenericConnection.crearConexion(url, user, pass);

        String sql ="select to_char(doc.fecrad,'dd-mm-yyyy') as FEC_RADICA,\n" +
                "       doc.nroradica as NRORADICA, substr(doc.usuario,5) as LIQUIDADOR,\n" +
                "       decode(pag.concepto,'D','DOCTO','M','MAY_VLR','OTROS') as CONCEPTO,\n" +
                "       pag.valor as VALOR_TOTAL_PAGO,\n" +
                "       substr(pag.CANAL||'-'||can.descripcion,1,40) as CANAL_PAGO,\n" +
                "       substr(pag.FORMA||'-'||tip.descripcion,1,40) as FORMA_PAGO,\n" +
                "       pag.FECHA as FEC_PAGO,\n" +
                "       pag.CHEQUE as CHEQUE_CONSIG, to_char(pag.APROBACION) as PIN_DOC_PAGO\n" +
                "from (select pag.nroradica, sum(pag.valor) valor, to_char(fecha,'dd-mm-yyyy') fecha, pag.concepto, pag.canal, pag.forma, pag.cheque, pag.aprobacion\n" +
                "  from pagos pag where concepto in ('D','M')\n" +
                "  and to_char(pag.fecsis,'mm-yyyy') = '%MONTH%-%YEAR%'\n" +
                "  group by pag.nroradica, to_char(fecha,'dd-mm-yyyy'), pag.concepto, pag.canal, pag.forma, pag.cheque, pag.aprobacion) pag, documentos doc, circulos cir, canales_pago can, tipo_pago tip\n" +
                "where local='S' and pag.nroradica=doc.nroradica\n" +
                "  and to_char(doc.fecrad,'mm-yyyy') = '%MONTH%-%YEAR%'\n" +
                "  and pag.canal=can.id_canal and pag.forma=tip.id_tipo_pago and anulado='S'\n" +
                "  UNION\n" +
                "select to_char(cer.fecsol,'dd-mm-yyyy') as FEC_RADICA,\n" +
                "  cer.nroradica as NRORADICA, cer.usuario as LIQUIDADOR,\n" +
                "  decode(pag.concepto,'C','CERTIF.','OTROS') as CONCEPTO,\n" +
                "  pag.valor as VALOR_TOTAL_PAGO,\n" +
                "  substr(pag.CANAL||'-'||can.descripcion,1,40) as CANAL_PAGO,\n" +
                "  substr(pag.FORMA||'-'||tip.descripcion,1,40) as FORMA_PAGO,\n" +
                "  pag.FECHA as FEC_PAGO,\n" +
                "  pag.CHEQUE as CHEQUE_CONSIG, to_char(pag.APROBACION) as PIN_DOC_PAGO\n" +
                "  from (select pag.nroradica, sum(pag.valor) valor, to_char(fecha,'dd-mm-yyyy') fecha, pag.concepto, pag.canal, pag.forma, pag.cheque, pag.aprobacion\n" +
                "  		from pagos pag where concepto in ('C')\n" +
                "  		and pag.fecsis between to_date('%DAYONE%-%MONTH%-%YEAR%','dd-mm-yyyy') and to_date('%DAYlAST%-%MONTH%-%YEAR%','dd-mm-yyyy')\n" +
                "  		group by pag.nroradica, to_char(fecha,'dd-mm-yyyy'), pag.concepto, pag.canal, pag.forma, pag.cheque, pag.aprobacion) pag, certificados cer, circulos cir, canales_pago can, tipo_pago tip\n" +
                "  where local='S' and pag.nroradica=cer.nroradica\n" +
                "  and cer.fecsol between to_date('%DAYONE%-%MONTH%-%YEAR%','dd-mm-yyyy') and to_date('%DAYlAST%-%MONTH%-%YEAR%','dd-mm-yyyy')\n" +
                "  and pag.canal=can.id_canal and pag.forma=tip.id_tipo_pago and anulado='S'";
        
        String dateString = fecha.toString();
        DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd", Locale.US); 
        LocalDate date = LocalDate.parse(dateString, dateFormat);
        LocalDate newDate = date.withDayOfMonth(date.getMonth().length(date.isLeapYear()));
        
        sql = sql.replace("%MONTH%", DateTimeFormatter.ofPattern("MM").format(fecha));
        sql = sql.replace("%YEAR%", String.valueOf(fecha.getYear()));
        sql = sql.replace("%DAYONE%", String.valueOf(fecha.getDayOfMonth()));
        sql = sql.replace("%DAYlAST%", String.valueOf(newDate.getDayOfMonth()));
        
        LOGGER.info("sql = " + sql);
        try (PreparedStatement ps = conn.prepareStatement(sql)) {
            ResultSet rs = ps.executeQuery();
            lista = ResultSetTransformer.transformarAnulados(rs);
            LOGGER.info( "RESULTADO", lista );

            rs.close();
            conn.close();
        }
        return lista;
    }
    
    
    
    public List<IngresoDto> obtenerOtrosConceptosPeriodo(String url, String user, String pass, LocalDate fecha) throws Exception {
        List<IngresoDto> lista;
        
        LOGGER.info("Obteniendo información de otros conceptos periodo");
        Connection conn = GenericConnection.crearConexion(url, user, pass);

        String sql ="select to_char(fecsis,'dd-mm-yyyy') as FEC_RADICA, pag.nroradica as NRORADICA,usuario as LIQUIDADOR,\n" +
                "       decode(pag.concepto,'O','OTROS_REC.','OTROS') as CONCEPTO,\n" +
                "       pag.valor as VALOR_PAGO, '' AS VALOR_DERECHOS, '' AS VLR_INCREMENTO,\n" +
                "       to_char(fecha,'dd-mm-yyyy') FEC_PAGO,\n" +
                "       substr(pag.FORMA||'-'||tip.descripcion,1,40) as FORMA_PAGO, substr(pag.CANAL||'-'||can.descripcion,1,40) as CANAL_PAGO,  '' AS FEC_SISTEMA,\n" +
                "       pag.CHEQUE as CHEQUE_CONSIG,\n" +
                "       to_char(pag.APROBACION) as PIN_DOC_PAGO\n" +
                "from pagos pag,\n" +
                "     canales_pago can, tipo_pago tip\n" +
                "where concepto in ('O')\n" +
                "  and to_char(pag.fecsis, 'mm-yyyy') = '%MONTH%-%YEAR%'\n" +
                "  and pag.canal=can.id_canal and pag.forma=tip.id_tipo_pago\n" +
                "group by pag.nroradica, to_char(pag.fecsis,'dd-mm-yyyy'), pag.usuario, pag.concepto,pag.valor,\n" +
                "substr(pag.CANAL||'-'||can.descripcion,1,40), substr(pag.FORMA||'-'||tip.descripcion,1,40), to_char(fecha,'dd-mm-yyyy'), pag.cheque, pag.aprobacion";
        
        sql = sql.replace("%MONTH%", DateTimeFormatter.ofPattern("MM").format(fecha));
        sql = sql.replace("%YEAR%", String.valueOf(fecha.getYear()));
        LOGGER.info("sql = " + sql);
        try (PreparedStatement ps = conn.prepareStatement(sql)) {
            ResultSet rs = ps.executeQuery();
            lista = ResultSetTransformer.transformarOtrosConceptos(rs);
            rs.close();
            conn.close();
        }
        return lista;
    }
    
    
    
    public List<IngresoDto> obtenerConsolidadoIncrementoPeriodo(String url, String user, String pass, LocalDate fecha) throws Exception {
        List<IngresoDto> lista;
        
        LOGGER.info("Obteniendo información de consolidado incremento periodo" );
        Connection conn = GenericConnection.crearConexion(url, user, pass);
        


        String sql ="select substr(cir.CODIGO||'-'||cir.NOMBRE,1,30) as CIRCULO_ORIP,\n" +
                "       to_char(doc.FECRAD,'mm-yyyy') as FECHA_TURNO,\n" +
                "       sum(valorliq) as VLR_LIQUIDACION,\n" +
                "       sum(valorinc) as VLR_RECAUDO_INC,\n" +
                "       substr(pag.canal||'-'||can.descripcion,1,40) as CANAL_PAGO,\n" +
                "       substr(pag.forma||'-'||tip.descripcion,1,40) as FORMA_PAGO, '' AS FEC_SISTEMA\n" +
                "from (select inc.nroradica,sum(inc.valorinc) valor, inc.canal canal, inc.forma forma\n" +
                "		from pagosinc inc where concepto in ('D','M')\n" +
                "		and to_char(inc.fecsis, 'mm-yyyy') = '%MONTH%-%YEAR%'\n" +
                "		group by inc.nroradica, inc.canal, inc.forma) pag, CIRCULOS cir, LIQUIDACIONINC inc, DOCUMENTOS doc, CANALES_PAGO can, TIPO_PAGO tip\n" +
                "where LOCAL='S' and to_char(doc.fecrad, 'mm-yyyy') = '%MONTH%-%YEAR%'\n" +
                "and inc.NRORADICA=doc.NRORADICA and inc.nroradica=pag.nroradica\n" +
                "and pag.canal=can.id_canal and pag.forma=tip.id_tipo_pago and (doc.ANULADO is NULL or doc.ANULADO='N')\n" +
                "group by substr(cir.CODIGO||'-'||cir.NOMBRE,1,30), substr(pag.canal||'-'||can.descripcion,1,40), pag.forma||'-'||tip.descripcion, to_char(doc.FECRAD,'mm-yyyy')\n";
        
        sql = sql.replace("%MONTH%", DateTimeFormatter.ofPattern("MM").format(fecha));
        sql = sql.replace("%YEAR%", String.valueOf(fecha.getYear()));
        
        LOGGER.info("sql = " + sql);
        try (PreparedStatement ps = conn.prepareStatement(sql)) {
            ResultSet rs = ps.executeQuery();
            lista = ResultSetTransformerConsoIncremento.transformar(rs);
            rs.close();
            conn.close();
        }
        return lista;
    }
    
    
    public List<IngresoDto> obtenerExentosPeriodo(String url, String user, String pass, LocalDate fecha) throws Exception {
        List<IngresoDto> lista;
        
        LOGGER.info("Obteniendo información de exentos periodo");
        Connection conn = GenericConnection.crearConexion(url, user, pass);

        String sql ="select to_char(doc.fecrad,'dd-mm-yyyy') as FEC_RADICA,\n" +
                "       doc.nroradica as NRORADICA,\n" +
                "       substr(doc.usuario,5) as LIQUIDADOR,\n" +
                "       decode(pag.concepto,'D','DOCTO') as CONCEPTO,\n" +
                "       decode(liq.tipoliq,'E','EXENTO','OTROS') as TIPO_LIQ, liq.liquidacion as VALOR_DERECHOS, inc.INCREMENTO as VLR_INCREMENTO,\n" +
                "       pag.FECSIS as FEC_SIS\n" +
                "from (select liq.nroradica, tipoliq, sum(valor) valor, sum(liq.liquidacion) liquidacion\n" +
                "  from liquidaciones liq where substr(liq.nroradica,1,4) = '%YEAR%'\n" +
                "  and tipoliq in ('E')\n" +
                "group by liq.nroradica, tipoliq) liq,\n" +
                "  	(select inc.nroradica,sum(inc.valorinc) INCREMENTO\n" +
                "  	from liquidacioninc inc where substr(inc.nroradica,1,4) = '%YEAR%'\n" +
                "  and tipoliq in ('E')\n" +
                "	group by inc.nroradica) inc,\n" +
                "  (select pag.nroradica,sum(pag.valor) valor, pag.concepto, to_char(fecsis,'dd-mm-yyyy') fecsis\n" +
                "  from pagos pag where concepto='D'\n" +
                "  and to_char(pag.fecsis,'mm-yyyy') = '%MONTH%-%YEAR%'\n" +
                "  group by pag.nroradica, pag.concepto, to_char(fecsis,'dd-mm-yyyy')) pag, documentos doc, circulos cir\n" +
                "  where local='S' and liq.nroradica=inc.nroradica and liq.nroradica=pag.nroradica and liq.nroradica=doc.nroradica\n" +
                "  and to_char(doc.fecrad,'mm-yyyy') = '%MONTH%-%YEAR%'\n" +
                "  and (anulado is null or anulado='N')\n" +
	            "  UNION\n" +
	            "select to_char(fecsis,'dd-mm-yyyy') as FEC_RADICA,\n" +
	            "  pag.nroradica as NRORADICA, usuario as LIQUIDADOR,\n" +
	            "  decode(pag.concepto,'C','CERTIF.','OTROS') as CONCEPTO,\n" +
	            "  'EXENTO' as TIPO_LIQ, pag.valor as VALOR_DERECHOS, 0 as VLR_INCREMENTO,\n" +
                "  to_char(fecsis,'dd-mm-yyyy') as FEC_SIS\n" +
	            "  from pagos pag, tipo_pago tip\n" +
	            "  		where concepto in ('C') \n" +
	            "  		and pag.nroradica in (select nroradica from certificados c where c.nroradica=pag.nroradica and tipo='E')\n" +
	            "  		and pag.nroradica not in (select nroradica from certificados c where c.nroradica=pag.nroradica and anulado='S')\n" +
	            "  		and fecsis>=to_date('%DAYONE%-%MONTH%-%YEAR%'||':0','dd-mm-yyyy:hh24')\n" +
	            "  		and fecsis<=to_date('%DAYlAST%-%MONTH%-%YEAR%'||':23:59','dd-mm-yyyy:hh24:MI')\n" +
	            "  		and pag.forma=tip.id_tipo_pago\n"+
	            "  		group by pag.nroradica, to_char(pag.fecsis,'dd-mm-yyyy'), pag.usuario, pag.concepto, pag.valor, to_char(fecha,'dd-mm-yyyy'), pag.aprobacion";
    
        
        String dateString = fecha.toString();
        DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd", Locale.US); 
        LocalDate date = LocalDate.parse(dateString, dateFormat);
        LocalDate newDate = date.withDayOfMonth(date.getMonth().length(date.isLeapYear()));
        
        sql = sql.replace("%MONTH%", DateTimeFormatter.ofPattern("MM").format(fecha));
        sql = sql.replace("%YEAR%", String.valueOf(fecha.getYear()));
        sql = sql.replace("%DAYONE%", String.valueOf(fecha.getDayOfMonth()));
        sql = sql.replace("%DAYlAST%", String.valueOf(newDate.getDayOfMonth()));
        
        LOGGER.info("sql = " + sql);
        try (PreparedStatement ps = conn.prepareStatement(sql)) {
            ResultSet rs = ps.executeQuery();
            lista = ResultSetTransformer.transformarExentos(rs);
            rs.close();
            conn.close();
        }
        return lista;
    }
    
    
    
    public List<IngresoDto> obtenerIngresosFPPeriodo(String url, String user, String pass, LocalDate fecha) throws Exception {
        List<IngresoDto> lista;
        
        LOGGER.info("Obteniendo información de ingresos fp periodo");
        Connection conn = GenericConnection.crearConexion(url, user, pass);

        String sql ="select to_char(doc.fecrad,'dd-mm-yyyy') as FEC_RADICA,\n" +
                "       doc.nroradica as NRORADICA, substr(doc.usuario,5) as LIQUIDADOR,\n" +
                "       decode(pag.concepto,'D','DOCTO','M','MAY_VLR','OTROS') as CONCEPTO,\n" +
                "       pag.valor as VALOR_TOTAL_PAGO,\n" +
                "       substr(pag.CANAL||'-'||can.descripcion,1,40) as CANAL_PAGO,\n" +
                "       substr(pag.FORMA||'-'||tip.descripcion,1,40) as FORMA_PAGO,\n" +
                "       pag.FECHA as FEC_PAGO,\n" +
                "       pag.CHEQUE as CHEQUE_CONSIG, to_char(pag.APROBACION) as PIN_DOC_PAGO\n" +
                "from (select pag.nroradica, sum(pag.valor) valor, to_char(fecha,'dd-mm-yyyy') fecha, pag.concepto, pag.canal, pag.forma, pag.cheque, pag.aprobacion\n" +
                "	from pagos pag where concepto in ('D','M')\n" +
                "  	and to_char(pag.fecsis,'mm-yyyy') = '%MONTH%-%YEAR%'\n" +
                "  	group by pag.nroradica, to_char(fecha,'dd-mm-yyyy'), pag.concepto, pag.canal, pag.forma, pag.cheque, pag.aprobacion) pag,\n" +
                "  	documentos doc, circulos cir, canales_pago can, tipo_pago tip\n" +
                "where local='S' and pag.nroradica=doc.nroradica\n" +
                "and to_char(doc.fecrad,'mm-yyyy') = '%MONTH%-%YEAR%'\n" +
                "and pag.canal=can.id_canal and pag.forma=tip.id_tipo_pago and (anulado is null or anulado='N')\n" +
                
                "  UNION\n" +
                "select to_char(cer.fecsol,'dd-mm-yyyy') as FEC_RADICA,\n" +
                "  cer.nroradica as NRORADICA, cer.usuario as LIQUIDADOR,\n" +
                "  decode(pag.concepto,'C','CERTIF.','V','MAY_VLR_C','OTROS') as CONCEPTO,\n" +
                "  pag.valor as VALOR_TOTAL_PAGO, \n" +
                "  substr(pag.CANAL||'-'||can.descripcion,1,40) as CANAL_PAGO,\n" +
                "  substr(pag.FORMA||'-'||tip.descripcion,1,40) as FORMA_PAGO,\n" +
                "  pag.FECHA as FEC_PAGO,\n" +
                "  pag.CHEQUE as CHEQUE_CONSIG, to_char(pag.APROBACION) as PIN_DOC_PAGO\n" +
                "  from (select pag.nroradica, sum(pag.valor) valor, to_char(fecha,'dd-mm-yyyy') fecha, pag.concepto, pag.canal, pag.forma, pag.cheque, pag.aprobacion\n" +
                "  		from pagos pag where concepto in ('C')\n" +
                "  		and pag.fecsis between to_date('%DAYONE%-%MONTH%-%YEAR%','dd-mm-yyyy') and to_date('%DAYlAST%-%MONTH%-%YEAR%','dd-mm-yyyy')\n" +
                "  		group by pag.nroradica, to_char(fecha,'dd-mm-yyyy'), pag.concepto, pag.canal, pag.forma, pag.cheque, pag.aprobacion) pag, certificados cer, circulos cir, canales_pago can, tipo_pago tip\n" +
                "  where local='S' and pag.nroradica=cer.nroradica\n" +
                "  and cer.fecsol between to_date('%DAYONE%-%MONTH%-%YEAR%','dd-mm-yyyy') and to_date('%DAYlAST%-%MONTH%-%YEAR%','dd-mm-yyyy')\n" +
                "  and pag.canal=can.id_canal and pag.forma=tip.id_tipo_pago and (anulado is null or anulado='N')";
        
        String dateString = fecha.toString();
        DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd", Locale.US); 
        LocalDate date = LocalDate.parse(dateString, dateFormat);
        LocalDate newDate = date.withDayOfMonth(date.getMonth().length(date.isLeapYear()));
        
        sql = sql.replace("%MONTH%", DateTimeFormatter.ofPattern("MM").format(fecha));
        sql = sql.replace("%YEAR%", String.valueOf(fecha.getYear()));
        sql = sql.replace("%DAYONE%", String.valueOf(fecha.getDayOfMonth()));
        sql = sql.replace("%DAYlAST%", String.valueOf(newDate.getDayOfMonth()));


        LOGGER.info("sql = " + sql);
        try (PreparedStatement ps = conn.prepareStatement(sql)) {
            ResultSet rs = ps.executeQuery();
            lista = ResultSetTransformer.transformarIngresosFP(rs);
            rs.close();
            conn.close();
        }
        return lista;
    }
    
    
    
    public List<IngresoDto> obtenerIngresosTRPeriodo(String url, String user, String pass, LocalDate fecha) throws Exception {
        List<IngresoDto> lista;
        
        LOGGER.info("Obteniendo información de ingresos tr periodo");
        Connection conn = GenericConnection.crearConexion(url, user, pass);

        String sql ="select to_char(doc.fecrad,'dd-mm-yyyy') as FEC_RADICA,\n" +
                "       doc.nroradica as NRORADICA, substr(doc.usuario,5) as LIQUIDADOR,\n" +
                "       decode(pag.concepto,'D','DOCTO') as CONCEPTO,\n" +
                "       liq.valor as VALOR_BASE,  liq.liquidacion as VALOR_DERECHOS, inc.INCREMENTO as VLR_INCREMENTO, '' as FEC_PAGO, pag.FECSIS as FEC_SISTEMA\n" +
                "from (select liq.nroradica, sum(valor) valor, sum(liq.liquidacion) liquidacion\n" +
                "     from liquidaciones liq where substr(liq.nroradica,1,4) = '%YEAR%'\n" +
                "  group by liq.nroradica) liq,\n" +
                "  (select inc.nroradica,sum(inc.valorinc) INCREMENTO\n" +
                "from liquidacioninc inc where substr(inc.nroradica,1,4) = '%YEAR%'\n" +
                "  group by inc.nroradica) inc,\n" +
                "  (select pag.nroradica,sum(pag.valor) valor, pag.concepto, to_char(fecsis,'dd-mm-yyyy') fecsis" +
                "  from pagos pag where concepto='D'\n" +
                "  and to_char(pag.fecsis,'mm-yyyy') = '%MONTH%-%YEAR%'\n" +
                "  group by pag.nroradica, pag.concepto, to_char(fecsis,'dd-mm-yyyy')) pag,\n" +
                "  documentos doc, circulos cir\n" +
                "where local='S' and liq.nroradica=inc.nroradica and liq.nroradica=pag.nroradica and liq.nroradica=doc.nroradica\n" +
                "  and to_char(doc.fecrad,'mm-yyyy') = '%MONTH%-%YEAR%'\n" +
                "  and (anulado is null or anulado='N')\n" +
                
                
                "  UNION\n" +
                "select to_char(fecsis,'dd-mm-yyyy') as FEC_RADICA,\n" +
                "  pag.nroradica as NRORADICA, usuario as LIQUIDADOR,\n" +
                "  decode(pag.concepto,'C','CERTIF.','OTROS') as CONCEPTO,\n" +
                "  0 as VALOR_BASE, pag.valor as VALOR_DERECHOS, 0 as VLR_INCREMENTO,  '' as FEC_PAGO, to_char(fecsis,'dd-mm-yyyy') as FEC_SISTEMA\n" +
                "  from pagos pag, tipo_pago tip\n" +
                "  where concepto in ('C') \n" +
                "  		and pag.nroradica not in (select nroradica from certificados c where c.nroradica=pag.nroradica and anulado='S')\n" +
                "  		and fecsis>=to_date('%DAYONE%-%MONTH%-%YEAR%'||':0','dd-mm-yyyy:hh24')\n" +
                "  		and fecsis<=to_date('%DAYlAST%-%MONTH%-%YEAR%'||':23:59','dd-mm-yyyy:hh24:MI') \n" +
                "  and pag.forma=tip.id_tipo_pago\n" +
                "  group by pag.nroradica, to_char(pag.fecsis,'dd-mm-yyyy'), pag.usuario, pag.concepto, pag.valor, to_char(fecha,'dd-mm-yyyy'), pag.aprobacion\n" +
                
                
		        "  UNION\n" +
		        "select to_char(PAG.fecsis,'dd-mm-yyyy') as FEC_RADICA,\n" +
		        "  pag.nroradica as NRORADICA, PAG.usuario as LIQUIDADOR,\n" +
		        "  decode(pag.concepto,'M','MAY_VAL.','OTROS') as CONCEPTO,\n" +
		        "  0 as VALOR_BASE, pag.valor - INC.VALORINC as VALOR_DERECHOS, INC.VALORINC as VLR_INCREMENTO,  '' as FEC_PAGO, to_char(PAG.fecsis,'dd-mm-yyyy') as FEC_SISTEMA\n" +
		        "  from pagos pag, PAGOSINC INC, tipo_pago tip\n" +
		        "  where PAG.NRORADICA=INC.NRORADICA AND PAG.concepto in ('M') AND PAG.CONCEPTO=INC.CONCEPTO\n" +
		        "  		and PAG.fecsis>=to_date('%DAYONE%-%MONTH%-%YEAR%'||':0','dd-mm-yyyy:hh24')\n" +
		        "  		and PAG.fecsis<=to_date('%DAYlAST%-%MONTH%-%YEAR%'||':23:59','dd-mm-yyyy:hh24:MI')\n" +
		        "  and pag.forma=tip.id_tipo_pago\n" +
		        "  group by pag.nroradica, to_char(pag.fecsis,'dd-mm-yyyy'), pag.usuario, pag.concepto, pag.valor, INC.VALORINC, to_char(PAG.fecha,'dd-mm-yyyy'), pag.aprobacion";
        
        String dateString = fecha.toString();
        DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd", Locale.US); 
        LocalDate date = LocalDate.parse(dateString, dateFormat);
        LocalDate newDate = date.withDayOfMonth(date.getMonth().length(date.isLeapYear()));
        
        sql = sql.replace("%MONTH%", DateTimeFormatter.ofPattern("MM").format(fecha));
        sql = sql.replace("%YEAR%", String.valueOf(fecha.getYear()));
        sql = sql.replace("%DAYONE%", String.valueOf(fecha.getDayOfMonth()));
        sql = sql.replace("%DAYlAST%", String.valueOf(newDate.getDayOfMonth()));
        
        
        LOGGER.info("sql = " + sql);
        try (PreparedStatement ps = conn.prepareStatement(sql)) {
            ResultSet rs = ps.executeQuery();
            lista = ResultSetTransformer.transformarIngresosTR(rs);
            rs.close();
            conn.close();
        }
        return lista;
    }
    
    
    
    public List<IngresoDto> obtenerResumenConsolidadoPeriodo(String url, String user, String pass, LocalDate fecha) throws Exception {
        List<IngresoDto> lista;
        
        LOGGER.info("Obteniendo información de resumen consolidado periodo");
        Connection conn = GenericConnection.crearConexion(url, user, pass);

		String sql = "(SELECT *\n" + "       FROM\n" + "       (SELECT codigo||'-'||nombre AS OFICINA,\n"
				+ "       		'01-01-2022' AS FEC_INI, '31-01-2022' AS FEC_FIN, 'LIQUIDACION PAGO' AS TABLA_BASE\n" 
				+ "       	        FROM circulos\n" + "		WHERE LOCAL='S') OFI,\n"
				+
				"		(SELECT count(c.nroradica) AS CANT_CERTIF,\n" + "		nvl(sum(valor), 0) AS VALOR_CERTIF\n"
				+ "		FROM certificados c\n" + "		WHERE recibo IS NOT NULL\n"
				+ "			AND fecsol>=to_date('01-01-2022' ||':0', 'dd-mm-yyyy:hh24')\n"
				+ "			AND fecsol<=to_date('31-01-2022'||':23:59', 'dd-mm-yyyy:hh24:MI')\n"
				+ "			AND (anulado IS NULL OR anulado='N')) T1,\n" +

				"		(SELECT count(DISTINCT nroradica) AS CANT_FOTOC,\n" + "		nvl(sum(valor), 0) AS VALOR_FOTOC\n"
				+ "		FROM consultas\n" + "		WHERE fecancel>=to_date('01-01-2022' ||':0', 'dd-mm-yyyy:hh24')\n"
				+ "			AND fecancel<=to_date('31-01-2022'||':23:59', 'dd-mm-yyyy:hh24:MI')\n"
				+ "			AND (anulado IS NULL OR anulado = 'N')\n" + "			AND tipo in ('P') ) T2,\n" +

				"		(SELECT count(DISTINCT d.nroradica) AS CANT_DOCTOS,\n"
				+ "		 nvl(sum(liquidacion), 0) AS VALOR_DOCTOS\n" + "		FROM documentos d, liquidaciones l\n"
				+ "		WHERE d.nroradica=l.nroradica\n" + "		AND l.liquidacion >= 0\n"
				+ "		AND ((fecrad>=to_date('01-01-2022' ||':0', 'dd-mm-yyyy:hh24')\n"
				+ "		AND fecrad<=to_date('31-01-2022'||':23:59', 'dd-mm-yyyy:hh24:MI')\n"
				+ "		AND (origen IS NULL OR origen in ('OR','R','RE')))\n"
				+ "		OR (fecvur>=to_date('01-01-2022' ||':0', 'dd-mm-yyyy:hh24')\n"
				+ "		AND fecvur<=to_date('31-01-2022'||':23:59', 'dd-mm-yyyy:hh24:MI')\n"
				+ "		AND origen = 'V'))\n" + "		AND (anulado IS NULL OR anulado='N') ) T3,\n" +

				"		(SELECT count(DISTINCT nroradica) AS CANT_MAYVR,\n" + "		nvl(sum(valor), 0) AS VLR_MAYVR\n"
				+ "		FROM diferliq\n" + "		WHERE fecancel>=to_date('01-01-2022' ||':0', 'dd-mm-yyyy:hh24')\n"
				+ "		AND fecancel<=to_date('31-01-2022'||':23:59', 'dd-mm-yyyy:hh24:MI')\n"
				+ "		AND tipo = 'I') T4,\n" +

				"		(SELECT count(DISTINCT nroradica) AS CANT_MAYVRC,\n"
				+ "		nvl(sum(valor), 0) AS VLR_MAYVRC\n" + "		FROM difercerti\n"
				+ "		WHERE fecancel>=to_date('01-01-2022' ||':0', 'dd-mm-yyyy:hh24')\n"
				+ "		AND fecancel<=to_date('31-01-2022'||':23:59', 'dd-mm-yyyy:hh24:MI')\n"
				+ "		AND tipo = 'I') T5,\n" +

				"		(SELECT count(DISTINCT nroradica) AS CANT_OTRRE,\n" + "		 nvl(sum(valor), 0) AS VLR_OTRRE\n"
				+ "		FROM otrosrec\n" + "		WHERE fecha>=to_date('01-01-2022' ||':0', 'dd-mm-yyyy:hh24')\n"
				+ "		AND fecha<=to_date('31-01-2022'||':23:59', 'dd-mm-yyyy:hh24:MI')\n"
				+ "		AND (anulado IS NULL\n" + "		 OR anulado = 'N') ) T6,\n" +

				"		(SELECT count(DISTINCT d.nroradica) AS CANT_DOCTO_ANUL,\n"
				+ "		 nvl(sum(liquidacion), 0) AS VLR_DOCTO_ANUL\n" + "		FROM documentos d,\n"
				+ "		liquidaciones l\n" + "		WHERE d.nroradica=l.nroradica\n" + "		AND l.liquidacion > 0\n"
				+ "		AND ((fecrad>=to_date('01-01-2022' ||':0', 'dd-mm-yyyy:hh24')\n"
				+ "		AND fecrad<=to_date('31-01-2022'||':23:59', 'dd-mm-yyyy:hh24:MI')\n"
				+ "		AND (origen IS NULL OR origen in ('OR','R','RE')))\n"
				+ "		OR (fecvur>=to_date('01-01-2022' ||':0', 'dd-mm-yyyy:hh24')\n"
				+ "		AND fecvur<=to_date('31-01-2022'||':23:59', 'dd-mm-yyyy:hh24:MI')\n"
				+ "		AND origen = 'V')) AND anulado='S') T8,\n" +

				"		(SELECT count(c.nroradica) AS CANT_CERTI_ANUL,\n"
				+ "		nvl(sum(valor), 0) AS VLR_CERTI_ANUL\n" + "		FROM certificados c\n"
				+ "		WHERE recibo IS NOT NULL\n" + "		AND valor >= 0\n"
				+ "		AND fecsol>=to_date('01-01-2022' ||':0', 'dd-mm-yyyy:hh24')\n"
				+ "		AND fecsol<=to_date('31-01-2022'||':23:59', 'dd-mm-yyyy:hh24:MI')\n"
				+ "		AND anulado='S') T9,\n" +
				
				"		(select count(distinct d.nroradica), nvl(sum(valorinc),0)\n"
				+ "		/*into wcantinccdd, wvlrinccdd  */\n" + "		from documentos d, liquidacioninc l\n"
				+ "		where d.nroradica=l.nroradica\n" + "		and l.valorinc >= 0\n" + "		and (\n"
				+ "		(fecrad>=to_date('01-01-2022' ||':0', 'dd-mm-yyyy:hh24') and\n"
				+ "		fecrad<=to_date('31-01-2022'||':23:59', 'dd-mm-yyyy:hh24:MI') and\n"
				+ "		(origen is null or origen in ('OR','R','RE')))\n" + "		or\n"
				+ "		(fecvur>=to_date('01-01-2022' ||':0', 'dd-mm-yyyy:hh24') and\n"
				+ "		fecvur<=to_date('31-01-2022'||':23:59', 'dd-mm-yyyy:hh24:MI') and\n" + "		origen = 'V')\n"
				+ "		)\n" + "		and (d.anulado is null or d.anulado='N')) T10,\n" +
				
				"		(select count(distinct nroradica), nvl(sum(valorinc),0)\n"
				+ "		/* into wcantinccdmp, wvlrinccdmp   */\n" + "		from pagosinc\n"
				+ "		where fecsis>=to_date('01-01-2022' ||':0', 'dd-mm-yyyy:hh24')\n"
				+ "		and fecsis<=to_date('31-01-2022'||':23:59', 'dd-mm-yyyy:hh24:MI')\n"
				+ "		and concepto = 'M') T11)\n"
				
				
				+ "		UNION\n" + "		(SELECT *\n" + "		FROM\n"
				+ "		(SELECT codigo||'-'||nombre AS OFICINA, '01-01-2022' AS FEC_INI, '31-01-2022' AS FEC_FIN,\n"
				+ "		'PAGOS X FEC_SIS' AS TABLA_BASE\n" + "		FROM circulos\n"
				+ "		WHERE LOCAL='S') OFI_PAGO,\n" +

				"		(SELECT count(DISTINCT p.nroradica) AS CANT_CERTIF,\n" + "		sum(p.valor) AS VALOR_CERTIF\n"
				+ "		FROM pagos p\n" + "		WHERE p.fecsis>=to_date('01-01-2022' ||':0', 'dd-mm-yyyy:hh24')\n"
				+ "		 AND p.fecsis<=to_date('31-01-2022'||':23:59', 'dd-mm-yyyy:hh24:MI')\n"
				+ "		AND concepto='C'\n" + "		AND p.valor >= 0\n" + "		AND nroradica not in\n"
				+ "		(SELECT c.nroradica\n" + "		FROM certificados c\n" + "		WHERE c.nroradica=p.nroradica\n"
				+ "		 AND (recibo IS NULL\n" + "		OR anulado='S')) ) T1_PAGO,\n" +

				"		(SELECT count(DISTINCT p.nroradica) AS CANT_FOTOC,\n"
				+ "		nvl(sum(p.valor), 0) AS VALOR_FOTOC\n" + "		FROM pagos p\n"
				+ "		WHERE p.fecsis>=to_date('01-01-2022' ||':0', 'dd-mm-yyyy:hh24')\n"
				+ "		AND p.fecsis<=to_date('31-01-2022'||':23:59', 'dd-mm-yyyy:hh24:MI')\n"
				+ "		AND concepto in ('P')\n" + "		AND nroradica not in\n" + "		(SELECT c.nroradica\n"
				+ "		FROM consultas c\n" + "		WHERE c.nroradica=p.nroradica\n" + "		AND (recibo IS NULL\n"
				+ "		OR anulado='S')) ) T2_PAGO,\n" +

				"		(SELECT count(DISTINCT p.nroradica) AS CANT_DOCTOS,\n"
				+ "		 nvl(sum(p.valor), 0) - nvl(sum(i.valorinc), 0) AS VALOR_DOCTOS\n" + "		FROM pagos p,\n"
				+ "pagosinc i\n" + "		WHERE p.nroradica=i.nroradica\n"
				+ "		AND (p.fecsis>=to_date('01-01-2022' ||':0', 'dd-mm-yyyy:hh24')\n"
				+ "		AND p.fecsis<=to_date('31-01-2022'||':23:59', 'dd-mm-yyyy:hh24:MI'))\n"
				+ "		AND p.concepto='D'\n" + "		AND p.concepto=i.concepto\n" + "		AND p.valor > 0\n"
				+ "		AND p.nroradica not in\n" + "		(SELECT b.nroradica\n" + "		FROM documentos b\n"
				+ "		WHERE b.nroradica=p.nroradica\n" + "		 AND (recibo IS NULL\n"
				+ "		 OR anulado='S')) ) T3_PAGO,\n" +

				"		(SELECT count(DISTINCT p.nroradica) AS CANT_MAYVR,\n"
				+ "		nvl(sum(p.valor), 0) - nvl(sum(i.valorinc), 0) AS VLR_MAYVR\n" + "		FROM pagos p,\n"
				+ "		pagosinc i\n" + "		WHERE p.nroradica=i.nroradica\n" + "		AND p.valor >= 0\n"
				+ "		AND i.valorinc >= 0\n" + "		AND p.fecsis>=to_date('01-01-2022' ||':0', 'dd-mm-yyyy:hh24')\n"
				+ "		AND p.fecsis<=to_date('31-01-2022'||':23:59', 'dd-mm-yyyy:hh24:MI')\n"
				+ "		AND p.concepto in ('M')\n" + "		AND p.concepto=i.concepto) T4_PAGO,\n" +

				"		(SELECT count(DISTINCT p.nroradica),\n"
				+ "		nvl(sum(p.valor), 0) /* into wcantmayvrcp, wvlrmayvrcp */\n" + "		FROM pagos p\n"
				+ "		WHERE p.valor >= 0\n" + "		AND p.fecsis>=to_date('01-01-2022' ||':0', 'dd-mm-yyyy:hh24')\n"
				+ "		AND p.fecsis<=to_date('31-01-2022'||':23:59', 'dd-mm-yyyy:hh24:MI')\n"
				+ "		AND p.concepto in ('V') ) T5_PAGO,\n" +

				"		(SELECT count(DISTINCT p.nroradica) AS CANT_MAYVRC,\n"
				+ "		 nvl(sum(p.valor), 0) AS VLR_MAYVRC /*into wcantotrrep, wvlrotrrep */\n"
				+ "		FROM pagos p\n" + "		WHERE p.fecsis>=to_date('01-01-2022' ||':0', 'dd-mm-yyyy:hh24')\n"
				+ "		AND p.fecsis<=to_date('31-01-2022'||':23:59', 'dd-mm-yyyy:hh24:MI')\n"
				+ "		AND concepto = 'O'\n" + "		AND p.valor >= 0\n" + "		AND p.nroradica not in\n"
				+ "		(SELECT o.nroradica\n" + "		FROM otrosrec o\n" + "		WHERE o.nroradica=p.nroradica\n"
				+ "		AND anulado='S') ) T6_PAGO,\n" +

				"		(SELECT count(DISTINCT p.nroradica) AS CANT_OTRRE,\n"
				+ "		nvl(sum(p.valor), 0) - nvl(sum(i.valorinc), 0) AS VLR_OTRRE /*into wcantdoctopan, wvlrdoctopan */\n"
				+ "		FROM pagos p,\n" + "		pagosinc i\n" + "		WHERE p.nroradica=i.nroradica\n"
				+ "		AND (p.fecsis>=to_date('01-01-2022' ||':0', 'dd-mm-yyyy:hh24')\n"
				+ "		AND p.fecsis<=to_date('31-01-2022'||':23:59', 'dd-mm-yyyy:hh24:MI'))\n"
				+ "		AND p.concepto='D'\n" + "		AND p.concepto=i.concepto\n"
				+ "		AND p.valor > 0 --and i.valorinc >= 0\n" +

				"		AND p.nroradica in\n" + "		(SELECT b.nroradica\n" + "		FROM documentos b\n"
				+ "		WHERE b.nroradica=p.nroradica\n" + "		AND anulado='S') ) T7_PAGO,\n" +

				"		(SELECT count(DISTINCT p.nroradica) AS CANT_CERTI_ANUL,\n"
				+ "		sum(p.valor) AS VLR_CERTI_ANUL /*into wcantcertipan, wvlrcertipan */\n" + "		FROM pagos p\n"
				+ "		WHERE p.fecsis>=to_date('01-01-2022' ||':0', 'dd-mm-yyyy:hh24')\n"
				+ "		AND p.fecsis<=to_date('31-01-2022'||':23:59', 'dd-mm-yyyy:hh24:MI')\n"
				+ "		AND concepto='C'\n" + "		AND p.valor >= 0\n" + "		AND nroradica in\n"
				+ "		(SELECT c.nroradica\n" + "		FROM certificados c\n" + "		WHERE c.nroradica=p.nroradica\n"
				+ "		AND anulado='S') ) T8_PAGO,\n" +
				
				"		( select count(distinct p.nroradica), nvl(sum(valorinc),0)\n"
				+ "		/*into wcantinccddp, wvlrinccddp */\n" + "		from pagosinc p\n"
				+ "		where (p.fecsis>=to_date('01-01-2022' ||':0', 'dd-mm-yyyy:hh24')\n"
				+ "		and p.fecsis<=to_date('31-01-2022'||':23:59', 'dd-mm-yyyy:hh24:MI'))\n"
				+ "		and concepto='D' and p.valorinc >= 0\n" + "		and nroradica not in (\n"
				+ "		select b.nroradica\n" + "		from documentos b\n" + "		where b.nroradica=p.nroradica\n"
				+ "		and anulado='S')) T9_PAGO,\n" +
				
				"		(select count(distinct nroradica), nvl(sum(valorinc),0)\n"
				+ "		/* into wcantinccdmp, wvlrinccdmp   */\n" + "		from pagosinc\n"
				+ "		where fecsis>=to_date('01-01-2022' ||':0', 'dd-mm-yyyy:hh24')\n"
				+ "		and fecsis<=to_date('31-01-2022'||':23:59', 'dd-mm-yyyy:hh24:MI')\n"
				+ "		and concepto = 'M') T10_PAGO)\n"
           
				
				+ "		UNION\n" + "		(SELECT *\n" + "		FROM\n"
				+ "		(SELECT codigo||'-'||nombre AS OFICINA,\n" + "'01-01-2022' AS FEC_INI, '31-01-2022' AS FEC_FIN,\n"
				+ "		'PAGOS X FEC_DOC_PAG' AS TABLA_BASE \n" + "		FROM circulos\n"
				+ "		WHERE LOCAL='S') OFI_X,\n" +

				"		(SELECT count(DISTINCT p.nroradica),\n"
				+ "		sum(p.valor) /*into wcantcertifp, wvlrcertifp*/\n" + "		FROM pagos p\n"
				+ "		WHERE p.fecha>=to_date('01-01-2022' ||':0', 'dd-mm-yyyy:hh24')\n"
				+ "		AND p.fecha<=to_date('31-01-2022'||':23:59', 'dd-mm-yyyy:hh24:MI')\n"
				+ "		AND concepto='C'\n" + "		AND p.valor >= 0\n" + "		AND nroradica not in\n"
				+ "		(SELECT c.nroradica\n" + "		FROM certificados c\n" + "		WHERE c.nroradica=p.nroradica\n"
				+ "		AND (recibo IS NULL\n" + "		 OR anulado='S')) ) T1_X,\n" +

				"		(SELECT count(DISTINCT p.nroradica),\n"
				+ "		 nvl(sum(p.valor), 0) /*into wcantfotocfp, wvlrfotocfp */\n" + "		FROM pagos p\n"
				+ "		WHERE p.fecha>=to_date('01-01-2022' ||':0', 'dd-mm-yyyy:hh24')\n"
				+ "		AND p.fecha<=to_date('31-01-2022'||':23:59', 'dd-mm-yyyy:hh24:MI')\n"
				+ "		AND concepto in ('P')\n" + "		AND nroradica not in\n" + "		(SELECT c.nroradica\n"
				+ "		FROM consultas c\n" + "		WHERE c.nroradica=p.nroradica\n" + "		AND (recibo IS NULL\n"
				+ "		OR anulado='S')) ) T2_X,\n" +

				"		(SELECT count(DISTINCT p.nroradica),\n"
				+ "		nvl(sum(p.valor), 0) - nvl(sum(i.valorinc), 0) /*into wcantdoctofp, wvlrdoctofp */\n"
				+ "		FROM pagos p,\n" + "		pagosinc i\n" + "		WHERE p.nroradica=i.nroradica\n"
				+ "		AND (p.fecha>=to_date('01-01-2022' ||':0', 'dd-mm-yyyy:hh24')\n"
				+ "		AND p.fecha<=to_date('31-01-2022'||':23:59', 'dd-mm-yyyy:hh24:MI'))\n"
				+ "		AND p.concepto='D'\n" + "		AND p.concepto=i.concepto\n"
				+ "		AND p.valor > 0 --and i.valorinc >= 0\n" +

				"		AND p.nroradica not in\n" + "		(SELECT b.nroradica\n" + "		FROM documentos b\n"
				+ "		WHERE b.nroradica=p.nroradica\n" + "		AND (recibo IS NULL\n"
				+ "		OR anulado='S')) ) T3_X,\n" +

				"		(SELECT count(DISTINCT p.nroradica),\n"
				+ "		nvl(sum(p.valor), 0) - nvl(sum(i.valorinc), 0) /*into wcantmayvrfp, wvlrmayvrfp */\n"
				+ "		FROM pagos p,\n" + "		pagosinc i\n" + "		WHERE p.nroradica=i.nroradica\n"
				+ "		AND p.valor >= 0\n" + "		AND i.valorinc >= 0\n"
				+ "		AND p.fecha>=to_date('01-01-2022' ||':0', 'dd-mm-yyyy:hh24')\n"
				+ "		AND p.fecha<=to_date('31-01-2022'||':23:59', 'dd-mm-yyyy:hh24:MI')\n"
				+ "		AND p.concepto in ('M')\n" + "		AND p.concepto=i.concepto) T4_X,\n" +

				"		(SELECT count(DISTINCT p.nroradica),\n"
				+ "		nvl(sum(p.valor), 0) /*into wcantmayvrcfp, wvlrmayvrcfp */\n" + "		FROM pagos p\n"
				+ "		WHERE p.valor >= 0\n" + "		AND p.fecha>=to_date('01-01-2022' ||':0', 'dd-mm-yyyy:hh24')\n"
				+ "		AND p.fecha<=to_date('31-01-2022'||':23:59', 'dd-mm-yyyy:hh24:MI')\n"
				+ "		AND p.concepto in ('V') ) T5_X,\n" +

				"		(SELECT count(DISTINCT p.nroradica),\n"
				+ "		nvl(sum(p.valor), 0) /*into wcantotrrefp, wvlrotrrefp */\n" + "		FROM pagos p\n"
				+ "		WHERE p.fecha>=to_date('01-01-2022' ||':0', 'dd-mm-yyyy:hh24')\n"
				+ "		AND p.fecha<=to_date('31-01-2022'||':23:59', 'dd-mm-yyyy:hh24:MI')\n"
				+ "		AND concepto = 'O'\n" + "		AND p.valor >= 0\n" + "		AND p.nroradica not in\n"
				+ "		(SELECT o.nroradica\n" + "		FROM otrosrec o\n" + "		WHERE o.nroradica=p.nroradica\n"
				+ "		AND anulado='S') ) T6_X,\n" +

				"		(SELECT count(DISTINCT p.nroradica),\n"
				+ "		nvl(sum(p.valor), 0) - nvl(sum(i.valorinc), 0) /*into wcantdoctofpan, wvlrdoctofpan */\n"
				+ "		FROM pagos p,\n" + "		pagosinc i\n" + "		WHERE p.nroradica=i.nroradica\n"
				+ "		AND (p.fecha>=to_date('01-01-2022' ||':0', 'dd-mm-yyyy:hh24')\n"
				+ "		AND p.fecha<=to_date('31-01-2022'||':23:59', 'dd-mm-yyyy:hh24:MI'))\n"
				+ "		AND p.concepto='D'\n" + "		AND p.concepto=i.concepto\n"
				+ "		AND p.valor > 0 --and i.valorinc >= 0\n" +

				"		AND p.nroradica in\n" + "		(SELECT b.nroradica\n" + "		FROM documentos b\n"
				+ "		WHERE b.nroradica=p.nroradica\n" + "		AND anulado='S') ) T8_X,\n" +

				"		(SELECT count(DISTINCT p.nroradica),\n"
				+ "		sum(p.valor) /*into wcantcertifpan, wvlrcertifpan */\n" + "		FROM pagos p\n"
				+ "		WHERE p.fecha>=to_date('01-01-2022' ||':0', 'dd-mm-yyyy:hh24')\n"
				+ "		AND p.fecha<=to_date('31-01-2022'||':23:59', 'dd-mm-yyyy:hh24:MI')\n"
				+ "		AND concepto='C'\n" + "		AND p.valor >= 0\n" + "		AND nroradica in\n"
				+ "		(SELECT c.nroradica\n" + "		FROM certificados c\n" + "		WHERE c.nroradica=p.nroradica\n"
				+ "		AND anulado='S') ) T9_X,\n" +
				
				"		(select count(distinct p.nroradica), nvl(sum(valorinc),0)\n"
				+ "		/*into wcantinccddfp, wvlrinccddfp   */\n" + "		from pagosinc p\n"
				+ "		where (p.fecha>=to_date('01-01-2022' ||':0', 'dd-mm-yyyy:hh24')\n"
				+ "		and p.fecha<=to_date('31-01-2022'||':23:59', 'dd-mm-yyyy:hh24:MI'))\n"
				+ "		and concepto='D' and p.valorinc >= 0\n" + "		and nroradica not in (\n"
				+ "		select b.nroradica\n" + "		from documentos b\n" + "		where b.nroradica=p.nroradica\n"
				+ "		and anulado='S')) T10_X,\n" +

				"		(select count(distinct nroradica), nvl(sum(valorinc),0)\n"
				+ "		/*into wcantinccdmfp, wvlrinccdmfp*/\n" + "		from pagosinc\n"
				+ "		where fecha>=to_date('01-01-2022' ||':0', 'dd-mm-yyyy:hh24')\n"
				+ "		and fecha<=to_date('31-01-2022'||':23:59', 'dd-mm-yyyy:hh24:MI')\n"
				+ "		and concepto = 'M') T11_X)\n";
        
        sql = sql.replace("%MONTH%", DateTimeFormatter.ofPattern("MM").format(fecha));
        sql = sql.replace("%YEAR%", String.valueOf(fecha.getYear()));
        LOGGER.info("sql = " + sql);
        try (PreparedStatement ps = conn.prepareStatement(sql)) {
            ResultSet rs = ps.executeQuery();
            lista = ResultSetTransformer.transformarResumen(rs);
            rs.close();
            conn.close();
        }
        return lista;
    }
    
    
    

}
