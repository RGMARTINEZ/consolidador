package model.report;

import model.excel.ExcelGenerator;

import java.io.File;
import java.util.List;

class ReportCreator {


    private final ExcelGenerator report;
    private final String nombreReporte;

    public ReportCreator(String nombreReporte) {
        this.nombreReporte = nombreReporte;
        report = new ExcelGenerator();
    }

    protected void crearHoja(String nombre, List<String> cabecera, List<Object> datos) throws Exception {
        report.generate(datos, cabecera, nombre);
    }

    protected void generarReporteCompleto() {
        File f = new File(nombreReporte);
        report.guardarReporte(f);
    }


}
