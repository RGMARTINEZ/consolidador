package model.database.sqlite;

import model.dto.IngresoDto;
import model.dto.IngresoReporteDto;

import java.sql.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class QueryManagerLite implements QueryManagerLiteInterface {


    @Override
    public void insertarFotocopias(List<IngresoDto> ingresos, String periodo, String codigoOficina, String nombreOficina) {
        String sql = "INSERT INTO con_fotocopias (ing_oficina, ing_nombre_oficina, ing_periodo, ing_fecha_arrastre, " +
                "   ing_fecha_turno, ing_turno, ing_liquidador, ing_concepto, ing_valor_base, ing_valor_derechos, " +
                "   ing_valor_incremento, ing_fecha_pago, ing_forma_pago, ing_canal_pago, ing_fecha_sistema, ing_cheque_consignacion, ing_pin_doc_pago) \n" +
                "   VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        Connection conn = ConnectionLite.connect();
        LocalDateTime fechaActual = LocalDateTime.now();
        ingresos.forEach(p -> {
            try {
                PreparedStatement ps = conn.prepareStatement(sql);
                ps.setString(1, codigoOficina);
                ps.setString(2, nombreOficina);
                ps.setString(3, periodo);
                ps.setDate(4, Date.valueOf(fechaActual.toLocalDate()));
                ps.setString(5, p.getFechaTurno());
                ps.setString(6, p.getTurno());
                ps.setString(7, p.getLiquidador());
                ps.setString(8, p.getConcepto());
                ps.setLong(9, p.getValorBase());
                ps.setLong(10, p.getValorDerechos());
                ps.setLong(11, p.getValorIncremento());
                ps.setString(12, p.getFechaPago());
                ps.setString(13, p.getFormaPago());
                ps.setString(14, p.getCanalPago());
                ps.setString(15, p.getFechaSistema());
                ps.setString(16, p.getChequeConsig());
                ps.setString(17, p.getPinDocPago());


//                    ps.setNull(9, Types.VARCHAR);
                ps.executeUpdate();
            } catch (SQLException e) {
                e.printStackTrace();
                cerrarConexion(conn);

            }
        });
        cerrarConexion(conn);
    }
    
    @Override
    public void insertarAnulados(List<IngresoDto> ingresos, String periodo, String codigoOficina, String nombreOficina) {
        String sql = "INSERT INTO con_anulados (ing_oficina, ing_nombre_oficina, ing_periodo, ing_fecha_arrastre, " +
                "   ing_fecha_turno, ing_turno, ing_liquidador, ing_concepto, ing_valor_base, " +
                "   ing_canal_pago, ing_forma_pago, ing_fecha_pago, ing_cheque_consignacion, ing_pin_doc_pago) \n" +
                "   VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        Connection conn = ConnectionLite.connect();
        LocalDateTime fechaActual = LocalDateTime.now();
        ingresos.forEach(p -> {
            try {
                PreparedStatement ps = conn.prepareStatement(sql);
                ps.setString(1, codigoOficina);
                ps.setString(2, nombreOficina);
                ps.setString(3, periodo);
                ps.setDate(4, Date.valueOf(fechaActual.toLocalDate()));
                ps.setString(5, p.getFechaTurno());
                ps.setString(6, p.getTurno());
                ps.setString(7, p.getLiquidador());
                ps.setString(8, p.getConcepto());
                ps.setLong(9, p.getValorBase());
                ps.setString(10, p.getCanalPago());
                ps.setString(11, p.getFormaPago());
                ps.setString(12, p.getFechaPago());
                ps.setString(13, p.getChequeConsig());
                ps.setString(14, p.getPinDocPago());

//                    ps.setNull(9, Types.VARCHAR);
                ps.executeUpdate();
            } catch (SQLException e) {
                e.printStackTrace();
                cerrarConexion(conn);

            }
        });
        cerrarConexion(conn);
    }
    
    
    @Override
    public void insertarOtrosConceptos(List<IngresoDto> ingresos, String periodo, String codigoOficina, String nombreOficina) {
        String sql = "INSERT INTO con_otros_conceptos (ing_oficina, ing_nombre_oficina, ing_periodo, ing_fecha_arrastre, " +
                "   ing_fecha_turno, ing_turno, ing_liquidador, ing_concepto, ing_valor_base, ing_valor_derechos, " +
                "   ing_valor_incremento, ing_fecha_pago, ing_forma_pago, ing_canal_pago, ing_fecha_sistema, ing_cheque_consignacion, ing_pin_doc_pago) \n" +
                "   VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        Connection conn = ConnectionLite.connect();
        LocalDateTime fechaActual = LocalDateTime.now();
        ingresos.forEach(p -> {
            try {
                PreparedStatement ps = conn.prepareStatement(sql);
                ps.setString(1, codigoOficina);
                ps.setString(2, nombreOficina);
                ps.setString(3, periodo);
                ps.setDate(4, Date.valueOf(fechaActual.toLocalDate()));
                ps.setString(5, p.getFechaTurno());
                ps.setString(6, p.getTurno());
                ps.setString(7, p.getLiquidador());
                ps.setString(8, p.getConcepto());
                ps.setLong(9, p.getValorBase());
                ps.setLong(10, p.getValorDerechos());
                ps.setLong(11, p.getValorIncremento());
                ps.setString(12, p.getFechaPago());
                ps.setString(13, p.getFormaPago());
                ps.setString(14, p.getCanalPago());
                ps.setString(15, p.getFechaSistema());
                ps.setString(16, p.getChequeConsig());
                ps.setString(17, p.getPinDocPago());
//                    ps.setNull(9, Types.VARCHAR);
                ps.executeUpdate();
            } catch (SQLException e) {
                e.printStackTrace();
                cerrarConexion(conn);

            }
        });
        cerrarConexion(conn);
    }
    
    
    @Override
    public void insertarConsolidadoIncremento(List<IngresoDto> ingresos, String periodo, String codigoOficina, String nombreOficina) {
        String sql = "INSERT INTO con_consolidado_incremento (ing_oficina, ing_nombre_oficina, ing_periodo, ing_fecha_arrastre, " +
                "   ing_circulo_orip, ing_fecha_turno, ing_valor_liquidacion, ing_valor_recaudo_inc, " +
                "   ing_canal_pago, ing_forma_pago, ing_fecha_sistema) \n" +
                "   VALUES (?,?,?,?,?,?,?,?,?,?,?)";
        Connection conn = ConnectionLite.connect();
        LocalDateTime fechaActual = LocalDateTime.now();
        ingresos.forEach(p -> {
            try {
                PreparedStatement ps = conn.prepareStatement(sql);
                ps.setString(1, codigoOficina);
                ps.setString(2, nombreOficina);
                ps.setString(3, periodo);
                ps.setDate(4, Date.valueOf(fechaActual.toLocalDate()));
                ps.setString(5, p.getCirculoOrip());
                ps.setString(6, p.getFechaTurno());
                ps.setLong(7, p.getValorLiquidacion());
                ps.setLong(8, p.getValorRecaudoInc());
                ps.setString(9, p.getCanalPago());
                ps.setString(10, p.getFormaPago());
                ps.setString(11, p.getFechaSistema());
//                    ps.setNull(9, Types.VARCHAR);
                ps.executeUpdate();
            } catch (SQLException e) {
                e.printStackTrace();
                cerrarConexion(conn);

            }
        });
        cerrarConexion(conn);
    }
    
    
    @Override
    public void insertarExentos(List<IngresoDto> ingresos, String periodo, String codigoOficina, String nombreOficina) {
        String sql = "INSERT INTO con_exentos (ing_oficina, ing_nombre_oficina, ing_periodo, ing_fecha_arrastre, " +
                "   ing_fecha_turno, ing_turno, ing_liquidador, ing_concepto, ing_tipo_liq, ing_valor_derechos, " +
                "   ing_valor_incremento, ing_fecha_sistema) \n" +
                "   VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";
        Connection conn = ConnectionLite.connect();
        LocalDateTime fechaActual = LocalDateTime.now();
        ingresos.forEach(p -> {
            try {
                PreparedStatement ps = conn.prepareStatement(sql);
                ps.setString(1, codigoOficina);
                ps.setString(2, nombreOficina);
                ps.setString(3, periodo);
                ps.setDate(4, Date.valueOf(fechaActual.toLocalDate()));
                ps.setString(5, p.getFechaTurno());
                ps.setString(6, p.getTurno());
                ps.setString(7, p.getLiquidador());
                ps.setString(8, p.getConcepto());
                ps.setString(9, p.getTipoLiq());
                ps.setLong(10, p.getValorDerechos());
                ps.setLong(11, p.getValorIncremento());
                ps.setString(12, p.getFechaSistema());

//                    ps.setNull(9, Types.VARCHAR);
                ps.executeUpdate();
            } catch (SQLException e) {
                e.printStackTrace();
                cerrarConexion(conn);

            }
        });
        cerrarConexion(conn);
    }
    
    
    @Override
    public void insertarIngresosFP(List<IngresoDto> ingresos, String periodo, String codigoOficina, String nombreOficina) {
        String sql = "INSERT INTO con_ingresos_fp (ing_oficina, ing_nombre_oficina, ing_periodo, ing_fecha_arrastre, " +
                "   ing_fecha_turno, ing_turno, ing_liquidador, ing_concepto, ing_valor_base, " +
                "   ing_fecha_pago, ing_forma_pago, ing_canal_pago, ing_cheque_consignacion, ing_pin_doc_pago) \n" +
                "   VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?, ?)";
        Connection conn = ConnectionLite.connect();
        LocalDateTime fechaActual = LocalDateTime.now();
        ingresos.forEach(p -> {
            try {
                PreparedStatement ps = conn.prepareStatement(sql);
                ps.setString(1, codigoOficina);
                ps.setString(2, nombreOficina);
                ps.setString(3, periodo);
                ps.setDate(4, Date.valueOf(fechaActual.toLocalDate()));
                ps.setString(5, p.getFechaTurno());
                ps.setString(6, p.getTurno());
                ps.setString(7, p.getLiquidador());
                ps.setString(8, p.getConcepto());
                ps.setLong(9, p.getValorBase());
                ps.setString(10, p.getFechaPago());
                ps.setString(11, p.getFormaPago());
                ps.setString(12, p.getCanalPago());
                ps.setString(13, p.getChequeConsig());
                ps.setString(14, p.getPinDocPago());
//                    ps.setNull(9, Types.VARCHAR);
                ps.executeUpdate();
            } catch (SQLException e) {
                e.printStackTrace();
                cerrarConexion(conn);

            }
        });
        cerrarConexion(conn);
    }
    
    
    @Override
    public void insertarIngresosTR(List<IngresoDto> ingresos, String periodo, String codigoOficina, String nombreOficina) {
        String sql = "INSERT INTO con_ingresos_tr (ing_oficina, ing_nombre_oficina, ing_periodo, ing_fecha_arrastre, " +
                "   ing_fecha_turno, ing_turno, ing_liquidador, ing_concepto, ing_valor_base, ing_valor_derechos, " +
                "   ing_valor_incremento, ing_fecha_sistema, ing_fecha_pago) \n" +
                "   VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)";
        Connection conn = ConnectionLite.connect();
        LocalDateTime fechaActual = LocalDateTime.now();
        ingresos.forEach(p -> {
            try {
                PreparedStatement ps = conn.prepareStatement(sql);
                ps.setString(1, codigoOficina);
                ps.setString(2, nombreOficina);
                ps.setString(3, periodo);
                ps.setDate(4, Date.valueOf(fechaActual.toLocalDate()));
                ps.setString(5, p.getFechaTurno());
                ps.setString(6, p.getTurno());
                ps.setString(7, p.getLiquidador());
                ps.setString(8, p.getConcepto());
                ps.setLong(9, p.getValorBase());
                ps.setLong(10, p.getValorDerechos());
                ps.setLong(11, p.getValorIncremento());
                ps.setString(12, p.getFechaSistema());
                ps.setString(13, p.getFechaPago());

//                    ps.setNull(9, Types.VARCHAR);
                ps.executeUpdate();
            } catch (SQLException e) {
                e.printStackTrace();
                cerrarConexion(conn);

            }
        });
        cerrarConexion(conn);
    }
    
    
    @Override
    public void insertarResumenConsolidado(List<IngresoDto> ingresos, String periodo, String codigoOficina, String nombreOficina) {
        String sql = "INSERT INTO con_resumen_consolidado (ing_oficina, ing_nombre_oficina, ing_periodo, ing_fecha_arrastre, " +
                "   ing_oficina_resumen, ing_fec_ini, ing_fec_fin, ing_tabla_base, ing_cant_certif, ing_valor_certif, " +
                "   ing_cant_fotoc, ing_valor_fotoc, ing_cant_doctos, ing_valor_doctos, ing_cant_mayvr, ing_valor_mayvr, ing_cant_mayvrc, ing_valor_mayvrc,  \n" +
                "   ing_cant_otrre, ing_valor_otrre, ing_cant_docto_anual, ing_valor_docto_anual, ing_cant_certi_anual, ing_valor_certi_anual, ing_valor_wcantinccdd, ing_valor_wvlrinccdd, ing_valor_wcantinccdmp, ing_valor_wvlrinccdmp ) \n" +
                "   VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,  ?,?,?,?)";
        Connection conn = ConnectionLite.connect();
        LocalDateTime fechaActual = LocalDateTime.now();
        ingresos.forEach(p -> {
            try {
                PreparedStatement ps = conn.prepareStatement(sql);
                ps.setString(1, codigoOficina);
                ps.setString(2, nombreOficina);
                ps.setString(3, periodo);
                ps.setDate(4, Date.valueOf(fechaActual.toLocalDate()));
                
                ps.setString(5, p.getOficina());
                ps.setString(6, p.getFecIni());
                ps.setString(7, p.getFecFin());
                ps.setString(8, p.getTablaBase());
                
                ps.setLong(9, p.getCantCertif());
                ps.setLong(10, p.getValorCertif());
                
                ps.setLong(11, p.getCantFotoc());
                ps.setLong(12, p.getValorFotoc());
                
                ps.setLong(13, p.getCantDoctos());
                ps.setLong(14, p.getValorDoctos());
                
                ps.setLong(15, p.getCantMayvr());
                ps.setLong(16, p.getValorMayvr());
                
                ps.setLong(17, p.getCantMayvrc());
                ps.setLong(18, p.getValorMayvrc());
                
                ps.setLong(19, p.getCantOtrre());
                ps.setLong(20, p.getValorOtrre());
                
                ps.setLong(21, p.getCantDoctoAnual());
                ps.setLong(22, p.getValorDoctoAnual());
                
                ps.setLong(23, p.getCantCertiAnual());
                ps.setLong(24, p.getValorCertiAnual());
                
                ps.setLong(25, p.getValorWcantinccdd());
                ps.setLong(26, p.getValorWvlrinccdd());

                
                ps.setLong(27, p.getValorWcantinccdmp());
                ps.setLong(28, p.getValorWvlrinccdmp());


//                    ps.setNull(9, Types.VARCHAR);
                ps.executeUpdate();
            } catch (SQLException e) {
                e.printStackTrace();
                cerrarConexion(conn);

            }
        });
        cerrarConexion(conn);
    }
    


    @Override
    public List<IngresoReporteDto> obtenerFotocopias(final String codigoOficina, final LocalDate fecha) {
        String sql = "SELECT ing_fecha_turno,\n" +
                "       ing_turno,\n" +
                "       ing_liquidador,\n" +
                "       ing_concepto as CONCEPTO,\n" +
                "   	ing_valor_base AS total,\n" +
                "       ing_canal_pago,\n" +    
                "       ing_forma_pago,\n" +   
                "       ing_fecha_pago,\n" +
                "       ing_cheque_consignacion,\n" +
                "       ing_pin_doc_pago\n" +
                "FROM con_fotocopias\n" +
                "	WHERE ing_oficina = ':codigoOficina'\n" +
                "  	AND ing_periodo = '%MONTH%-%YEAR%'\n" ;
        sql = obtenerQueryReemplazado(sql, fecha, codigoOficina);
        System.out.println("---------- FOTOCOPIAS = " + sql);
        return realizarConsultaFotocopias(sql);
    }


    
    @Override
    public List<IngresoReporteDto> obtenerAnulados(final String codigoOficina, final LocalDate fecha) {
        String sql = "SELECT ing_fecha_turno,\n" +
                "       ing_turno,\n" +
                "       ing_liquidador,\n" +
                "       ing_concepto as CONCEPTO,\n" +
                "   	ing_valor_base AS total,\n" +
                "       ing_fecha_pago,\n" +
                "       ing_forma_pago,\n" +   
                "       ing_canal_pago,\n" +        
                "       ing_fecha_sistema,\n" +                
                "       ing_cheque_consignacion,\n" +
                "       ing_pin_doc_pago\n" +
                "FROM con_anulados\n" +
		        "	WHERE ing_oficina = ':codigoOficina'\n" +
		        "  	AND ing_periodo = '%MONTH%-%YEAR%'\n" ;
		        sql = obtenerQueryReemplazado(sql, fecha, codigoOficina);
        System.out.println("---------- ANULADOS = " + sql);
        return realizarConsultaIngresos(sql);
    }
    
    
    
    @Override
    public List<IngresoReporteDto> obtenerOtrosConceptos(final String codigoOficina, final LocalDate fecha) {
        String sql = "SELECT ing_fecha_turno,\n" +
                "       ing_turno,\n" +
                "       ing_liquidador,\n" +
                "       ing_concepto as CONCEPTO,\n" +
                "   	ing_valor_base AS total,\n" +
                "   	ing_valor_derechos AS valor_derechos,\n" +
                "       ing_valor_incremento AS valor_incremento,\n" +
                "       ing_fecha_pago,\n" +
                "       ing_forma_pago,\n" +   
                "       ing_canal_pago,\n" +        
                "       ing_fecha_sistema,\n" +                
                "       ing_cheque_consignacion,\n" +
                "       ing_pin_doc_pago\n" +
                "FROM con_otros_conceptos\n" +
		        "	WHERE ing_oficina = ':codigoOficina'\n" +
		        "  	AND ing_periodo = '%MONTH%-%YEAR%'\n" ;
        sql = obtenerQueryReemplazado(sql, fecha, codigoOficina);
        System.out.println("---------- OTROS CONCEPTOS = " + sql);
        return realizarConsultaIngresos(sql);
    }
    
    
    
    @Override
    public List<IngresoReporteDto> obtenerConsolidadoIncremento(String codigoOficina, LocalDate fecha) {
        String sql = "SELECT ing_circulo_orip,\n" +
                "       ing_fecha_turno,\n" +
                "       ing_valor_liquidacion,\n" +
                "       ing_valor_recaudo_inc,\n" +
                "   	ing_canal_pago,\n" +
                "   	ing_forma_pago\n" +
                "FROM con_consolidado_incremento\n" +
		        "	WHERE ing_oficina = ':codigoOficina'\n" +
		        "  	AND ing_periodo = '%MONTH%-%YEAR%'\n" ;
        sql = obtenerQueryReemplazado(sql, fecha, codigoOficina);
        System.out.println("---------- CONSOLIDADO INCREMENTO = " + sql);
        return realizarConsultaConcolidadoIncremento(sql);
    }
    
    
    @Override
    public List<IngresoReporteDto> obtenerExentos(final String codigoOficina, final LocalDate fecha) {
        String sql = "SELECT ing_fecha_turno,\n" +
                "       ing_turno,\n" +
                "       ing_liquidador,\n" +
                "       ing_concepto as CONCEPTO,\n" +
                "   	ing_tipo_liq,\n" +
                "   	ing_valor_derechos AS valor_derechos,\n" +
                "       ing_valor_incremento AS valor_incremento,\n" +
                "       ing_fecha_sistema\n" +
                "FROM con_exentos\n" +
		        "	WHERE ing_oficina = ':codigoOficina'\n" +
		        "  	AND ing_periodo = '%MONTH%-%YEAR%'\n" ;
        sql = obtenerQueryReemplazado(sql, fecha, codigoOficina);
//        System.out.println("sql otros recaudos = " + sql);
        return realizarConsultaExentos(sql);
    }

    
    @Override
    public List<IngresoReporteDto> obtenerIngresosFP(final String codigoOficina, final LocalDate fecha) {
        String sql = "SELECT ing_fecha_turno,\n" +
                "       ing_turno,\n" +
                "       ing_liquidador,\n" +
                "       ing_concepto as CONCEPTO,\n" +
                "   	ing_valor_base AS total,\n" +
                "       ing_fecha_pago,\n" +
                "       ing_forma_pago,\n" +   
                "       ing_canal_pago,\n" +        
                "       ing_cheque_consignacion,\n" +
                "       ing_pin_doc_pago\n" +
                "FROM con_ingresos_fp\n" +
		        "	WHERE ing_oficina = ':codigoOficina'\n" +
		        "  	AND ing_periodo = '%MONTH%-%YEAR%'\n" ;
        sql = obtenerQueryReemplazado(sql, fecha, codigoOficina);
//        System.out.println("sql otros recaudos = " + sql);
        return realizarConsultaIngresosFP(sql);
    }
    
    
    @Override
    public List<IngresoReporteDto> obtenerIngresosTR(final String codigoOficina, final LocalDate fecha) {
        String sql = "SELECT ing_fecha_turno,\n" +
                "       ing_turno,\n" +
                "       ing_liquidador,\n" +
                "       ing_concepto as CONCEPTO,\n" +
                "   	ing_valor_base AS total,\n" +
                "   	ing_valor_derechos AS valor_derechos,\n" +
                "       ing_valor_incremento AS valor_incremento,\n" +
                "       ing_fecha_pago\n" +
                "FROM con_ingresos_tr\n" +
		        "	WHERE ing_oficina = ':codigoOficina'\n" +
		        "  	AND ing_periodo = '%MONTH%-%YEAR%'\n" ;
        sql = obtenerQueryReemplazado(sql, fecha, codigoOficina);
//        System.out.println("sql otros recaudos = " + sql);
        return realizarConsultaIngresosTR(sql);
    }
    
    
    @Override
    public List<IngresoReporteDto> obtenerResumenConsolidado(final String codigoOficina, final LocalDate fecha) {
        String sql = "SELECT ing_oficina_resumen,\n" 
                + "	ing_fec_ini,\n"
                + "	ing_fec_fin,\n"
                + "	ing_tabla_base,\n"
                + "	ing_cant_certif,\n"
                + "	ing_valor_certif,\n"
                + "	ing_cant_fotoc,\n"
                + "	ing_valor_fotoc,\n"
                
                + "	ing_cant_doctos,\n"
                + "	ing_valor_doctos,\n"
                + "	ing_cant_mayvr,\n"
                + "	ing_valor_mayvr,\n"
                + "	ing_cant_mayvrc,\n"
                + "	ing_valor_mayvrc,\n"
                + "	ing_cant_otrre,\n"
                + "	ing_valor_otrre,\n"
                + "	ing_cant_docto_anual,\n"
                + "	ing_valor_docto_anual,\n"
                + "	ing_cant_certi_anual,\n"
                + "	ing_valor_certi_anual,\n" 
                
                + "	ing_valor_wcantinccdd, \n" 
                + "	ing_valor_wvlrinccdd, \n" 
                + "	ing_valor_wcantinccdmp, \n" 
                + "	ing_valor_wvlrinccdmp \n" +


                
            
                "FROM con_resumen_consolidado\n" +
		        "	WHERE ing_periodo = '%MONTH%-%YEAR%'\n" ;
        sql = obtenerQueryReemplazado(sql, fecha, codigoOficina);
//        System.out.println("sql otros recaudos = " + sql);
        return realizarConsultaResumenConsolidado(sql);
    }
    
    
    private List<IngresoReporteDto> realizarConsultaIngresos(String sql){
        Connection conn = ConnectionLite.connect();
        List<IngresoReporteDto> ingresos = new ArrayList<>();
        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            ingresos = obtenerIngresos(rs);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return ingresos;
    }
    
    
    private List<IngresoReporteDto> realizarConsultaFotocopias(String sql){
        Connection conn = ConnectionLite.connect();
        List<IngresoReporteDto> ingresos = new ArrayList<>();
        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            ingresos = obtenerFotocopias(rs);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return ingresos;
    }
    
    
    private List<IngresoReporteDto> realizarConsultaConcolidadoIncremento(String sql){
        Connection conn = ConnectionLite.connect();
        List<IngresoReporteDto> ingresos = new ArrayList<>();
        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            ingresos = ingresosConsolidadoIncremento(rs);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return ingresos;
    }
    
    
    private List<IngresoReporteDto> realizarConsultaExentos(String sql){
        Connection conn = ConnectionLite.connect();
        List<IngresoReporteDto> ingresos = new ArrayList<>();
        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            ingresos = ingresosExentos(rs);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return ingresos;
    }
    
    
    private List<IngresoReporteDto> realizarConsultaIngresosFP(String sql){
        Connection conn = ConnectionLite.connect();
        List<IngresoReporteDto> ingresos = new ArrayList<>();
        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            ingresos = ingresosFP(rs);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return ingresos;
    }
    
    
    private List<IngresoReporteDto> realizarConsultaIngresosTR(String sql){
        Connection conn = ConnectionLite.connect();
        List<IngresoReporteDto> ingresos = new ArrayList<>();
        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            ingresos = ingresosTR(rs);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return ingresos;
    }
    
    private List<IngresoReporteDto> realizarConsultaResumenConsolidado(String sql){
        Connection conn = ConnectionLite.connect();
        List<IngresoReporteDto> ingresos = new ArrayList<>();
        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            ingresos = ingresosResumenConsolidado(rs);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return ingresos;
    }
    
    
    
    

    /**
     * Reemplaza los valores para la consulta con los necesarios
     * @param sql Sql a reemplazar
     * @param fecha Periodo necesario para reemplazar
     * @param codigoOficina Oficina asociada
     * @return Query listo para ser ejecutado
     */
    private String obtenerQueryReemplazado(String sql, LocalDate fecha, String codigoOficina){
        sql = sql.replace("%MONTH%", DateTimeFormatter.ofPattern("MM").format(fecha));
        sql = sql.replace("%YEAR%", String.valueOf(fecha.getYear()));
        sql = sql.replace(":codigoOficina", codigoOficina);
        return sql;
    }

    /**
     * Dado un resultset, devuelve la lista de ingresos internos
     * @param rs Resultset a extraer
     * @return Lista de Ingresos obtenidos de la base de datos
     * @throws SQLException Error al obtener alguna de las columnas requeridas
     */
    private List<IngresoReporteDto> obtenerIngresos(ResultSet rs) throws SQLException {
        List<IngresoReporteDto> ingresos = new ArrayList<>();
        IngresoReporteDto ingreso;
        while(rs.next()){
            ingreso = new IngresoReporteDto();
            ingreso.setFechaTurno(rs.getString("ING_FECHA_TURNO"));
            ingreso.setTurno(rs.getString("ING_TURNO"));
            ingreso.setLiquidador(rs.getString("ING_LIQUIDADOR"));
            ingreso.setConcepto(rs.getString("CONCEPTO"));
            if(isThere(rs, "TOTAL")){
                ingreso.setValorTotal(rs.getLong("TOTAL"));
            }
            if(isThere(rs, "VALOR_DERECHOS")){
                ingreso.setValorDerechos(rs.getLong("VALOR_DERECHOS"));
            }
            if(isThere(rs, "VALOR_INCREMENTO")){
                ingreso.setValorIncremento(rs.getLong("VALOR_INCREMENTO"));
            }

            ingreso.setFechaPago(rs.getString("ING_FECHA_PAGO"));
            ingreso.setFormaPago(rs.getString("ING_FORMA_PAGO"));
            ingreso.setCanalPago(rs.getString("ING_CANAL_PAGO"));
            ingreso.setFechaSistema(rs.getString("ING_FECHA_SISTEMA"));
            ingreso.setChequeConsig(rs.getString("ING_CHEQUE_CONSIGNACION"));
            ingreso.setPinDocPago(rs.getString("ING_PIN_DOC_PAGO"));

            ingresos.add(ingreso);
        }
        return ingresos;
    }
    
    
    private List<IngresoReporteDto> obtenerFotocopias(ResultSet rs) throws SQLException {
        List<IngresoReporteDto> ingresos = new ArrayList<>();
        IngresoReporteDto ingreso;
        while(rs.next()){
            ingreso = new IngresoReporteDto();
            ingreso.setFechaTurno(rs.getString("ING_FECHA_TURNO"));
            ingreso.setTurno(rs.getString("ING_TURNO"));
            ingreso.setLiquidador(rs.getString("ING_LIQUIDADOR"));
            ingreso.setConcepto(rs.getString("CONCEPTO"));
            if(isThere(rs, "TOTAL")){
                ingreso.setValorTotal(rs.getLong("TOTAL"));
            }

            ingreso.setCanalPago(rs.getString("ING_CANAL_PAGO"));
            ingreso.setFormaPago(rs.getString("ING_FORMA_PAGO"));
            ingreso.setFechaPago(rs.getString("ING_FECHA_PAGO"));
            ingreso.setChequeConsig(rs.getString("ING_CHEQUE_CONSIGNACION"));
            ingreso.setPinDocPago(rs.getString("ING_PIN_DOC_PAGO"));


            ingresos.add(ingreso);
        }
        return ingresos;
    }
    
    
    
    private List<IngresoReporteDto> ingresosConsolidadoIncremento(ResultSet rs) throws SQLException {
        List<IngresoReporteDto> ingresos = new ArrayList<>();
        IngresoReporteDto ingreso;
        while(rs.next()){
            ingreso = new IngresoReporteDto();
            ingreso.setCirculoOrip(rs.getString("ING_CIRCULO_ORIP"));
            ingreso.setFechaTurno(rs.getString("ING_FECHA_TURNO"));
            ingreso.setValorLiquidacion(rs.getLong("ING_VALOR_LIQUIDACION"));
            ingreso.setValorRecaudoInc(rs.getLong("ING_VALOR_RECAUDO_INC"));
            ingreso.setCanalPago(rs.getString("ING_CANAL_PAGO"));
            ingreso.setFormaPago(rs.getString("ING_FORMA_PAGO"));
            ingresos.add(ingreso);
        }
        return ingresos;
    }
    
    
    private List<IngresoReporteDto> ingresosExentos(ResultSet rs) throws SQLException {
        List<IngresoReporteDto> ingresos = new ArrayList<>();
        IngresoReporteDto ingreso;
        while(rs.next()){
            ingreso = new IngresoReporteDto();
            ingreso.setFechaTurno(rs.getString("ING_FECHA_TURNO"));
            ingreso.setTurno(rs.getString("ING_TURNO"));
            ingreso.setLiquidador(rs.getString("ING_LIQUIDADOR"));
            ingreso.setConcepto(rs.getString("CONCEPTO"));
            ingreso.setTipoLiq(rs.getString("ING_TIPO_LIQ"));
            if(isThere(rs, "VALOR_DERECHOS")){
                ingreso.setValorDerechos(rs.getLong("VALOR_DERECHOS"));
            }
            if(isThere(rs, "VALOR_INCREMENTO")){
                ingreso.setValorIncremento(rs.getLong("VALOR_INCREMENTO"));
            }
            ingreso.setFechaSistema(rs.getString("ING_FECHA_SISTEMA"));
            
            ingresos.add(ingreso);
        }
        return ingresos;
    }
    
    
    private List<IngresoReporteDto> ingresosFP(ResultSet rs) throws SQLException {
        List<IngresoReporteDto> ingresos = new ArrayList<>();
        IngresoReporteDto ingreso;
        while(rs.next()){
            ingreso = new IngresoReporteDto();
            ingreso.setFechaTurno(rs.getString("ING_FECHA_TURNO"));
            ingreso.setTurno(rs.getString("ING_TURNO"));
            ingreso.setLiquidador(rs.getString("ING_LIQUIDADOR"));
            ingreso.setConcepto(rs.getString("CONCEPTO"));
            if(isThere(rs, "TOTAL")){
                ingreso.setValorTotal(rs.getLong("TOTAL"));
            }
            ingreso.setCanalPago(rs.getString("ING_CANAL_PAGO"));
            ingreso.setFormaPago(rs.getString("ING_FORMA_PAGO"));
            ingreso.setChequeConsig(rs.getString("ING_CHEQUE_CONSIGNACION"));
            ingreso.setPinDocPago(rs.getString("ING_PIN_DOC_PAGO"));
            ingresos.add(ingreso);
        }
        return ingresos;
    }
    
    
    private List<IngresoReporteDto> ingresosTR(ResultSet rs) throws SQLException {
        List<IngresoReporteDto> ingresos = new ArrayList<>();
        IngresoReporteDto ingreso;
        while(rs.next()){
            ingreso = new IngresoReporteDto();
            ingreso.setFechaTurno(rs.getString("ING_FECHA_TURNO"));
            ingreso.setTurno(rs.getString("ING_TURNO"));
            ingreso.setLiquidador(rs.getString("ING_LIQUIDADOR"));
            ingreso.setConcepto(rs.getString("CONCEPTO"));
            if(isThere(rs, "TOTAL")){
                ingreso.setValorTotal(rs.getLong("TOTAL"));
            }
            if(isThere(rs, "VALOR_DERECHOS")){
                ingreso.setValorDerechos(rs.getLong("VALOR_DERECHOS"));
            }
            if(isThere(rs, "VALOR_INCREMENTO")){
                ingreso.setValorIncremento(rs.getLong("VALOR_INCREMENTO"));
            }
            ingreso.setFechaPago(rs.getString("ING_FECHA_PAGO"));


            ingresos.add(ingreso);
        }
        return ingresos;
    }
    
    
    private List<IngresoReporteDto> ingresosResumenConsolidado(ResultSet rs) throws SQLException {
        List<IngresoReporteDto> ingresos = new ArrayList<>();
        IngresoReporteDto ingreso;
        while(rs.next()){
            ingreso = new IngresoReporteDto();
            
            ingreso.setOficina(rs.getString("ing_oficina_resumen"));
            ingreso.setFecIni(rs.getString("ing_fec_ini"));
            ingreso.setFecFin(rs.getString("ing_fec_fin"));
            ingreso.setTablaBase(rs.getString("ing_tabla_base"));

            
            ingreso.setCantCertif(rs.getLong("ing_cant_certif"));
            ingreso.setValorCertif(rs.getLong("ing_valor_certif"));
            
            ingreso.setCantFotoc(rs.getLong("ing_cant_fotoc"));
            ingreso.setValorFotoc(rs.getLong("ing_valor_fotoc"));
            
            ingreso.setCantDoctos(rs.getLong("ing_cant_doctos"));
            ingreso.setValorDoctos(rs.getLong("ing_valor_doctos"));
            
            ingreso.setCantMayvr(rs.getLong("ing_cant_mayvr"));
            ingreso.setValorMayvr(rs.getLong("ing_valor_mayvr"));
            
            ingreso.setCantMayvrc(rs.getLong("ing_cant_mayvrc"));
            ingreso.setValorMayvrc(rs.getLong("ing_valor_mayvrc"));
            
            ingreso.setCantOtrre(rs.getLong("ing_cant_otrre"));
            ingreso.setValorOtrre(rs.getLong("ing_valor_otrre"));
            
            ingreso.setCantDoctoAnual(rs.getLong("ing_cant_docto_anual"));
            ingreso.setValorDoctoAnual(rs.getLong("ing_valor_docto_anual"));
            
            ingreso.setCantCertiAnual(rs.getLong("ing_cant_certi_anual"));
            ingreso.setValorCertiAnual(rs.getLong("ing_valor_certi_anual"));
            
            ingreso.setValorWcantinccdd(rs.getLong("ing_valor_wcantinccdd"));
            ingreso.setValorWvlrinccdd(rs.getLong("ing_valor_wvlrinccdd"));
            
            ingreso.setValorWcantinccdmp(rs.getLong("ing_valor_wcantinccdmp"));
            ingreso.setValorWvlrinccdmp(rs.getLong("ing_valor_wvlrinccdmp"));


            



            ingresos.add(ingreso);
        }
        return ingresos;
    }

    /**
     * Comprueba si una columna existe
     * @param rs Result set que puede contener la columna
     * @param column Nombre de la columna a verificar
     * @return Existe o no
     */
    private boolean isThere(ResultSet rs, String column){
        try{
            rs.findColumn(column);
            return true;
        } catch (SQLException sqlex){
//            System.out.println("No existe columna: " + column);
        }

        return false;
    }

    private void cerrarConexion(Connection conn) {
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }

}
