package model.report;

import model.database.sqlite.QueryManagerLite;

import model.database.sqlite.QueryManagerLiteInterface;
import model.dto.ConexionUrlDto;
import model.dto.IngresoReporteDto;
import model.manager.ConsolidatorManager;

import java.time.LocalDate;
import java.time.format.TextStyle;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import com.google.gson.Gson;


public class DataOrganizer {

    private static final QueryManagerLiteInterface consultasLite;

    static {
        consultasLite = new QueryManagerLite();
    }

    private List<String> cabeceraFotocopias;
    private List<String> cabeceraAnulados;
    private List<String> cabeceraOtrosConceptos;
    private List<String> cabeceraConsolidadoIncremento;
    private List<String> cabeceraExentos;
    private List<String> cabeceraIngresosFP;
    private List<String> cabeceraIngresosTR;
    private List<String> cabeceraResumenConsolidado;



    private ReportCreator reportCreator;
    

    
    public void generarReportesDetalladosFotocopias(String codigoOficina, LocalDate periodo) throws Exception {
    	
        /*** Cabecera Fotocopias ***/
    	
        cabeceraFotocopias = Arrays.asList("FEC_RADICA", "NRORADICA", "LIQUIDADOR", "CONCEP", "VALOR_PAGO", "CANAL_PAGO", "FORMA_PAGO", "FEC_PAGO", "CHEQUE_CONSIG", "PIN_DOC_PAGO");
        
    	cabeceraConsolidadoIncremento = Arrays.asList("CIRCULO_ORIP", "PERIODO", "VLR_LIQUIDACION", "VLR_RECAUDO_INC", "CANAL_PAGO", "FORMA_PAGO");
    	
        cabeceraExentos = Arrays.asList("FEC_RADICA", "NRORADICA", "LIQUIDADOR", "CONCEPT", "TIPO_L", "VALOR_DERECHOS", "VALOR_INCREMENTO",  "FEC_SIS");
        
        cabeceraAnulados = Arrays.asList("FEC_RADICA", "NRORADICA", "LIQUIDADOR", "CONCEP", "VALOR_TOTAL_PAGO", "CANAL_PAGO", "FORMA_PAGO", "FEC_PAGO", "CHEQUE_CONSIG", "PIN_DOC_PAGO");
        
        cabeceraIngresosFP = Arrays.asList("FEC_RADICA", "NRORADICA", "LIQUIDADOR", "CONCEPT", "VALOR_TOTAL_PAGO", "CANAL_PAGO", "FORMA_PAGO", "FEC_PAGO", "CHEQUE_CONSIG", "PIN_DOC_PAGO");
        
        cabeceraIngresosTR = Arrays.asList("FEC_RADICA", "NRORADICA", "LIQUIDADOR", "CONCEPTO", "VALOR_BASE", "VALOR_DERECHOS", "VALOR_INCREMENTO", "FEC_SIS");
        



        
        //reportCreator = new ReportCreator("ReporteDetalle-Fotocopias" + "-" + periodo.getMonthValue() + "-" +
                //periodo.getYear() + ".xlsx");
        
        reportCreator = new ReportCreator("ReporteDetalle-Anulados" + "-" + periodo.getMonthValue() + "-" + periodo.getYear() + ".xlsx");
        //reportCreator = new ReportCreator("ReporteDetalle-Otros-Conceptos" + "-" + periodo.getMonthValue() + "-" + periodo.getYear() + ".xlsx");
        //reportCreator = new ReportCreator("ReporteDetalle-Exentos" + "-" + periodo.getMonthValue() + "-" + periodo.getYear() + ".xlsx");
        //reportCreator = new ReportCreator("ReporteDetalle-IngresosFP" + "-" + periodo.getMonthValue() + "-" + periodo.getYear() + ".xlsx");
        //reportCreator = new ReportCreator("ReporteDetalle-IngresosTR" + "-" + periodo.getMonthValue() + "-" + periodo.getYear() + ".xlsx");
        //reportCreator = new ReportCreator("ReporteDetalle-Consolidado_Incremento" + "-" + periodo.getMonthValue() + "-" + periodo.getYear() + ".xlsx");





        
    	String[] months = new String[] { "01"};

    	Gson gson = new Gson();
    	String json = 
    			"[ "
  
    					

                        + "{ "
                        + " \"url\" : \"jdbc:oracle:thin:@192.168.108.21:1521:nrte\", "
                        + " \"nombre\" : \"BOGOTA NORTE\","
                        + " \"codigo\" : \"50N\" "
                        + "},"
    					+ "{ "
    					+ " \"url\" : \"jdbc:oracle:thin:@192.168.108.18:16200:ctro\", "
    					+ " \"nombre\" : \"BOGOTA CENTRO\","
    					+ " \"codigo\" : \"50C\" "
    					+ "}"
 
    					+ "]"
    					
    					;
		ConexionUrlDto[] conexiones = gson.fromJson(json, ConexionUrlDto[].class);
			

    	for ( ConexionUrlDto urls : conexiones) {
    		//generarReporteFotocopias(urls.getCodigo(), periodo, urls.getNombre()); 
    		//generarReporteOtrosConceptos(urls.getCodigo(), periodo, urls.getNombre());    
    		//generarReporteConsolidadoIncremento(urls.getCodigo(), periodo, urls.getNombre());  
    		//generarReporteExentos(urls.getCodigo(), periodo, urls.getNombre());  
    		generarReporteAnulados(urls.getCodigo(), periodo, urls.getNombre()); 
    		//generarReporteIngresosFP(urls.getCodigo(), periodo, urls.getNombre());
    		//generarReporteIngresosTR(urls.getCodigo(), periodo, urls.getNombre());    






    	}
        reportCreator.generarReporteCompleto();
    }



    
    
    public void generarReportesDetallados(String codigoOficina, LocalDate periodo) throws Exception {
    	
  
    	
        cabeceraResumenConsolidado = Arrays.asList("OFICINA", "FEC_INI", "FEC_FIN", "TABLA_BASE", "cant_certif", "valor_certif", "cant_fotoc", "valor_fotoc", "cant_doctos",
        											"valor_doctos", "cant_mayvr", "vlr_mayvr", "cant_mayvrc", "vlr_mayvrc", "cant_otrre", "vlr_otrre", "VALOR_INCCDOC", "cant_docto_anul", "vlr_docto_anul", "cant_certi_anul", "vlr_certi_anul");

        
        
        reportCreator = new ReportCreator("ReporteDetalle-Resumen_Consolidado" + "-" +
                periodo.getYear() + "-" + periodo.getMonth().getDisplayName(TextStyle.FULL, new Locale("es-CO")) + ".xlsx");
        
        
        
    	
    	//String[] months = new String[] { "11","12" };
    	
    	String[] months = new String[] { "01"};

    	
    	
    	
    	Gson gson = new Gson();
    	String json = 
    			"[ "
  
    					

                        + "{ "
                        + " \"url\" : \"jdbc:oracle:thin:@192.168.108.21:1521:nrte\", "
                        + " \"nombre\" : \"BOGOTA NORTE\","
                        + " \"codigo\" : \"50N\" "
                        + "},"
    					+ "{ "
    					+ " \"url\" : \"jdbc:oracle:thin:@192.168.108.18:16200:ctro\", "
    					+ " \"nombre\" : \"BOGOTA CENTRO\","
    					+ " \"codigo\" : \"50C\" "
    					+ "}"
 
    					+ "]"
    					
    					;
			ConexionUrlDto[] conexiones = gson.fromJson(json, ConexionUrlDto[].class);
	    	 generarReporteResumenConsolidado("", periodo, "");

		

        reportCreator.generarReporteCompleto();
    }



    private void generarReporteFotocopias(String codigoOficina, LocalDate periodo, String nombreOficina) throws Exception {
        System.out.println("GENERANDO REPORTE DETALLADO FOTOCOPIAS PARA: " + codigoOficina);
        List<IngresoReporteDto> fotocopias = consultasLite.obtenerFotocopias(codigoOficina, periodo);
        reportCreator.crearHoja(nombreOficina, cabeceraFotocopias, convertirListaFotocopias(fotocopias));
    }

    
    private void generarReporteAnulados(String codigoOficina, LocalDate periodo, String nombreOficina) throws Exception {
        System.out.println("GENERANDO REPORTE DETALLADO ANULADOS FP PARA: " + codigoOficina);
        List<IngresoReporteDto> anulados = consultasLite.obtenerAnulados(codigoOficina, periodo);
        reportCreator.crearHoja(nombreOficina, cabeceraAnulados, convertirListaGenerica(anulados));
    }
    
    
    private void generarReporteOtrosConceptos(String codigoOficina, LocalDate periodo, String nombreOficina) throws Exception {
        System.out.println("GENERANDO REPORTE DETALLADO OTRCONC PARA : " + codigoOficina);
        List<IngresoReporteDto> otrosConceptos = consultasLite.obtenerOtrosConceptos(codigoOficina, periodo);
        reportCreator.crearHoja(nombreOficina, cabeceraFotocopias, convertirListaGenerica(otrosConceptos));
    }
    
   
    
    private void generarReporteExentos(String codigoOficina, LocalDate periodo, String nombreOficina) throws Exception {
        System.out.println("GENERANDO REPORTE DETALLADO EXENTOS PARA: " + codigoOficina);
        List<IngresoReporteDto> exentos = consultasLite.obtenerExentos(codigoOficina, periodo);
        reportCreator.crearHoja(nombreOficina, cabeceraExentos, convertirExentos(exentos));
    }
    
    
    private void generarReporteConsolidadoIncremento(String codigoOficina, LocalDate periodo, String nombreOficina) throws Exception {
        System.out.println("GENERANDO REPORTE CONSOLIDADO INCREMENTO PARA: " + codigoOficina);
        List<IngresoReporteDto> consolidadoIncremento = consultasLite.obtenerConsolidadoIncremento(codigoOficina, periodo);
        reportCreator.crearHoja(nombreOficina, cabeceraConsolidadoIncremento, convertirConsolidadoIncremento(consolidadoIncremento));
    }
    
    
    private void generarReporteIngresosFP(String codigoOficina, LocalDate periodo, String nombreOficina) throws Exception {
        System.out.println("GENERANDO REPORTE DETALLADO INGRESOS_FP PARA: " + codigoOficina);
        List<IngresoReporteDto> ingresosFP = consultasLite.obtenerIngresosFP(codigoOficina, periodo);
        reportCreator.crearHoja(nombreOficina, cabeceraIngresosFP, convertirIngresosFP(ingresosFP));
    }
    
    
    private void generarReporteIngresosTR(String codigoOficina, LocalDate periodo, String nombreOficina) throws Exception {
        System.out.println("GENERANDO REPORTE DETALLADO INGRESOS TR PARA: " + codigoOficina);
        List<IngresoReporteDto> ingresosTR = consultasLite.obtenerIngresosTR(codigoOficina, periodo);
        reportCreator.crearHoja(nombreOficina, cabeceraIngresosTR, convertirIngresosTR(ingresosTR));
    }
    
    
    private void generarReporteResumenConsolidado(String codigoOficina, LocalDate periodo, String nombreOficina) throws Exception {
        System.out.println("GENERANDO REPORTE RESUMEN CONSOLIDADO PARA: " + codigoOficina);
        List<IngresoReporteDto> resumenConsolidado = consultasLite.obtenerResumenConsolidado(codigoOficina, periodo);
        reportCreator.crearHoja("REPORTE_RESUMEN_CONSOLIDADO", cabeceraResumenConsolidado, convertirResumenConsolidado(resumenConsolidado));
    }
    
    
    

    private List<Object> convertirLista(List<IngresoReporteDto> ingresos) {
        List<Object> listaGenerica = new ArrayList<>();
        if (ingresos.size() > 0) {
            for (IngresoReporteDto ingreso : ingresos) {
                Object[] datosFila = {ingreso.getFechaTurno(), ingreso.getTurno(), ingreso.getLiquidador(),
                        ingreso.getConcepto(), ingreso.getValorDerechos(), ingreso.getValorIncremento(), ingreso.getValorTotal(),
                        ingreso.getFormaPago(), ingreso.getFechaPago()};
                listaGenerica.add(datosFila);
            }
        } else {
            Object[] datosFila = {"", "", "",
                    "", "", "", "",
                    "", ""};
            listaGenerica.add(datosFila);
        }

        return listaGenerica;
    }
    
    
    private List<Object> convertirListaGenerica(List<IngresoReporteDto> ingresos) {
        List<Object> listaGenerica = new ArrayList<>(); 
        if (ingresos.size() > 0) {
            for (IngresoReporteDto ingreso : ingresos) {
                Object[] datosFila = {ingreso.getFechaTurno(), ingreso.getTurno(), ingreso.getLiquidador(),
                        ingreso.getConcepto(), ingreso.getValorTotal(), ingreso.getCanalPago(), ingreso.getFormaPago(), ingreso.getFechaPago(), ingreso.getChequeConsig(), ingreso.getPinDocPago() };
                listaGenerica.add(datosFila);
            }
        } else {
            Object[] datosFila = {"", "", "", "", "", "", "", "", "", ""};
            listaGenerica.add(datosFila);
        }
        return listaGenerica;
    }
    
    private List<Object> convertirListaFotocopias(List<IngresoReporteDto> ingresos) {
        List<Object> listaGenerica = new ArrayList<>(); 
        if (ingresos.size() > 0) {
            for (IngresoReporteDto ingreso : ingresos) {
                Object[] datosFila = {ingreso.getFechaTurno(), ingreso.getTurno(), ingreso.getLiquidador(),
                        ingreso.getConcepto(), ingreso.getValorTotal(), ingreso.getCanalPago(), ingreso.getFormaPago(), ingreso.getFechaPago(), ingreso.getChequeConsig(), ingreso.getPinDocPago() };
                listaGenerica.add(datosFila);
            }
        } else {
            Object[] datosFila = {"", "", "", "", "", "", "", "", "", ""};
            listaGenerica.add(datosFila);
        }
        return listaGenerica;
    }
    

    
    private List<Object> convertirExentos(List<IngresoReporteDto> ingresos) {
        List<Object> listaGenerica = new ArrayList<>(); 
        if (ingresos.size() > 0) {
            for (IngresoReporteDto ingreso : ingresos) {
                Object[] datosFila = {ingreso.getFechaTurno(), ingreso.getTurno(), ingreso.getLiquidador(),
                        ingreso.getConcepto(), ingreso.getTipoLiq(), ingreso.getValorDerechos(), ingreso.getValorIncremento(), ingreso.getFechaSistema() };
                listaGenerica.add(datosFila);
            }
        } else {
            Object[] datosFila = {"", "", "", "", "", "", "", ""};
            listaGenerica.add(datosFila);
        }
        return listaGenerica;
    }
    
    private List<Object> convertirIngresosFP(List<IngresoReporteDto> ingresos) {
        List<Object> listaGenerica = new ArrayList<>(); 
        if (ingresos.size() > 0) {
            for (IngresoReporteDto ingreso : ingresos) {
                Object[] datosFila = {ingreso.getFechaTurno(), ingreso.getTurno(), ingreso.getLiquidador(),
                        ingreso.getConcepto(), ingreso.getValorTotal(), ingreso.getCanalPago(), ingreso.getFormaPago(), ingreso.getFechaPago(), ingreso.getChequeConsig(), ingreso.getPinDocPago() };
                listaGenerica.add(datosFila);
            }
        } else {
            Object[] datosFila = {"", "", "", "", "", "", "", "", "", ""};
            listaGenerica.add(datosFila);
        }
        return listaGenerica;
    }
    
    
    private List<Object> convertirIngresosTR(List<IngresoReporteDto> ingresos) {
        List<Object> listaGenerica = new ArrayList<>(); 
        if (ingresos.size() > 0) {
            for (IngresoReporteDto ingreso : ingresos) {
                Object[] datosFila = {ingreso.getFechaTurno(), ingreso.getTurno(), ingreso.getLiquidador(),
                        ingreso.getConcepto(), ingreso.getValorTotal(),  ingreso.getValorDerechos(), ingreso.getValorIncremento(), ingreso.getFechaPago() };
                listaGenerica.add(datosFila);
            }
        } else {
            Object[] datosFila = {"", "", "", "", "", "", "", ""};
            listaGenerica.add(datosFila);
        }
        return listaGenerica;
    }
    
    private List<Object> convertirConsolidadoIncremento(List<IngresoReporteDto> ingresos) {
        List<Object> listaGenerica = new ArrayList<>(); 
        if (ingresos.size() > 0) {
            for (IngresoReporteDto ingreso : ingresos) {
                Object[] datosFila = {ingreso.getCirculoOrip(), ingreso.getFechaTurno(), ingreso.getValorLiquidacion(),
                        ingreso.getValorRecaudoInc(), ingreso.getCanalPago(), ingreso.getFormaPago() };
                listaGenerica.add(datosFila);
            }
        } else {
            Object[] datosFila = {"", "", "", "", "", ""};
            listaGenerica.add(datosFila);
        }
        return listaGenerica;
    }
    
    
    private List<Object> convertirResumenConsolidado(List<IngresoReporteDto> ingresos) {
        List<Object> listaGenerica = new ArrayList<>(); 
        if (ingresos.size() > 0) {
            for (IngresoReporteDto ingreso : ingresos) {
                Object[] datosFila = {ingreso.getOficina(), ingreso.getFecIni(), ingreso.getFecFin(),
                        ingreso.getTablaBase(), ingreso.getCantCertif(), ingreso.getValorCertif(), ingreso.getCantFotoc(), ingreso.getValorFotoc(), ingreso.getCantDoctos(), ingreso.getValorDoctos(),
                ingreso.getCantMayvr(), ingreso.getValorMayvr(), ingreso.getCantMayvrc(), ingreso.getValorMayvrc(), ingreso.getCantOtrre(), ingreso.getValorOtrre(), (ingreso.getValorWvlrinccdd() + ingreso.getValorWvlrinccdmp()), ingreso.getCantDoctoAnual(), ingreso.getValorDoctoAnual(), ingreso.getCantCertiAnual(), ingreso.getValorCertiAnual()};

                listaGenerica.add(datosFila);
            }
        } else {
            Object[] datosFila = {"", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", ""};
            listaGenerica.add(datosFila);
        }
        return listaGenerica;
    }
    
    
    
    
    
}
