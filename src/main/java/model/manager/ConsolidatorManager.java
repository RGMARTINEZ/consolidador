package model.manager;

import model.database.oracle.QueryManagerFolio;
import model.database.sqlite.QueryManagerLite;
import model.database.sqlite.QueryManagerLiteInterface;
import model.dto.IngresoDto;
import model.report.DataOrganizer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;

public class ConsolidatorManager {
	
    //private static final Logger LOGGER = LoggerFactory.getLogger(ConsolidatorManager.class);


    public void generarInformacion(String codigo, int year, int month, String nombreOficina, String url) throws Exception {
        LocalDate fechaPeriodo = LocalDate.of(year, month, 1);
        QueryManagerFolio queryManagerFolio = new QueryManagerFolio();
 



        String user = "OPS$REPORDAF";
        String pass = "REPORDAF";
        
       //List<IngresoDto> fotocopias = queryManagerFolio.obtenerFotocopias(url, user, pass, fechaPeriodo);
       
       List<IngresoDto> anulados = queryManagerFolio.obtenerAnuladosPeriodo(url, user, pass, fechaPeriodo);
       
       //List<IngresoDto> otrosConceptos = queryManagerFolio.obtenerOtrosConceptosPeriodo(url, user, pass, fechaPeriodo);
       
       //List<IngresoDto> consolidadoIncremento = queryManagerFolio.obtenerConsolidadoIncrementoPeriodo(url, user, pass, fechaPeriodo);
        
       //List<IngresoDto> exentos = queryManagerFolio.obtenerExentosPeriodo(url, user, pass, fechaPeriodo);
        
       //List<IngresoDto> ingresosFP = queryManagerFolio.obtenerIngresosFPPeriodo(url, user, pass, fechaPeriodo);
        
       //List<IngresoDto> ingresosTR = queryManagerFolio.obtenerIngresosTRPeriodo(url, user, pass, fechaPeriodo);
        
       //List<IngresoDto> resumenConsolidado = queryManagerFolio.obtenerResumenConsolidadoPeriodo(url, user, pass, fechaPeriodo);

       
       System.out.println("---------- INICIO DE INSERCION DE DATOS SQLITE ----------");


       QueryManagerLiteInterface queryManagerLite = new QueryManagerLite();
      
       //queryManagerLite.insertarFotocopias(fotocopias, fechaPeriodo.format(DateTimeFormatter.ofPattern("MM-yyyy")), codigo, nombreOficina);
       
       queryManagerLite.insertarAnulados(anulados, fechaPeriodo.format(DateTimeFormatter.ofPattern("MM-yyyy")), codigo, nombreOficina);
       
       //queryManagerLite.insertarOtrosConceptos(otrosConceptos, fechaPeriodo.format(DateTimeFormatter.ofPattern("MM-yyyy")), codigo, nombreOficina);

       //queryManagerLite.insertarConsolidadoIncremento(consolidadoIncremento, fechaPeriodo.format(DateTimeFormatter.ofPattern("MM-yyyy")), codigo, nombreOficina);
       
       //queryManagerLite.insertarExentos(exentos, fechaPeriodo.format(DateTimeFormatter.ofPattern("MM-yyyy")), codigo, nombreOficina);
       
       //queryManagerLite.insertarIngresosFP(ingresosFP, fechaPeriodo.format(DateTimeFormatter.ofPattern("MM-yyyy")), codigo, nombreOficina);
       
       //queryManagerLite.insertarIngresosTR(ingresosTR, fechaPeriodo.format(DateTimeFormatter.ofPattern("MM-yyyy")), codigo, nombreOficina);
       
       //queryManagerLite.insertarResumenConsolidado(resumenConsolidado, fechaPeriodo.format(DateTimeFormatter.ofPattern("MM-yyyy")), codigo, nombreOficina);

       //DataOrganizer dataOrganizer = new DataOrganizer();
       //dataOrganizer.generarReportesDetallados(codigo, fechaPeriodo);
       System.out.println("---------- INGRESO DE INFORMACION CORRECTAMENTE ----------" + nombreOficina);
    }
    
    
    public void generarReportesTabs(String codigo, int year, int month, String nombreOficina, String url) throws Exception {
       LocalDate fechaPeriodo = LocalDate.of(year, month, 1);
       QueryManagerFolio queryManagerFolio = new QueryManagerFolio();
       DataOrganizer dataOrganizer = new DataOrganizer();
       dataOrganizer.generarReportesDetalladosFotocopias(codigo, fechaPeriodo);
       System.out.println("---------- REPORTES DETALLADOS GENERADOS CORRECTAMENTE ----------");
    }

}
