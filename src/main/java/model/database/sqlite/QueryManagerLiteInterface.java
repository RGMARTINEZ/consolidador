package model.database.sqlite;

import model.dto.IngresoDto;
import model.dto.IngresoReporteDto;

import java.time.LocalDate;
import java.util.List;

public interface QueryManagerLiteInterface {


    /**
     * Inserta los ingresos en la tabla CON_FOTOCOPIAS
     * @param ingresos Ingresos obtenidos como lista
     * @param periodo Periodo definido en formato MM-YYYY
     * @param codigoOficina Codigo de la oficina de registro
     * @param nombreOficina Nombre de la oficina
     */
    void insertarFotocopias(List<IngresoDto> ingresos, String periodo, String codigoOficina, String nombreOficina);
    
    /**
     * Inserta los ingresos en la tabla CON_ANULADOS
     * @param ingresos Ingresos obtenidos como lista
     * @param periodo Periodo definido en formato MM-YYYY
     * @param codigoOficina Codigo de la oficina de registro
     * @param nombreOficina Nombre de la oficina
     */
    void insertarAnulados(List<IngresoDto> ingresos, String periodo, String codigoOficina, String nombreOficina);
    
    
    /**
     * Inserta los ingresos en la tabla CON_OTROS_CONCEPTOS
     * @param ingresos Ingresos obtenidos como lista
     * @param periodo Periodo definido en formato MM-YYYY
     * @param codigoOficina Codigo de la oficina de registro
     * @param nombreOficina Nombre de la oficina
     */
    void insertarOtrosConceptos(List<IngresoDto> ingresos, String periodo, String codigoOficina, String nombreOficina);
    
    
    /**
     * Inserta los ingresos en la tabla CON_CONSOLIDADO_INCREMENTO
     * @param ingresos Ingresos obtenidos como lista
     * @param periodo Periodo definido en formato MM-YYYY
     * @param codigoOficina Codigo de la oficina de registro
     * @param nombreOficina Nombre de la oficina
     */
    void insertarConsolidadoIncremento(List<IngresoDto> ingresos, String periodo, String codigoOficina, String nombreOficina);
    
    
    /**
     * Inserta los ingresos en la tabla CON_EXENTOS
     * @param ingresos Ingresos obtenidos como lista
     * @param periodo Periodo definido en formato MM-YYYY
     * @param codigoOficina Codigo de la oficina de registro
     * @param nombreOficina Nombre de la oficina
     */
    void insertarExentos(List<IngresoDto> ingresos, String periodo, String codigoOficina, String nombreOficina);
    
    
    /**
     * Inserta los ingresos en la tabla CON_INGRESOS_FP
     * @param ingresos Ingresos obtenidos como lista
     * @param periodo Periodo definido en formato MM-YYYY
     * @param codigoOficina Codigo de la oficina de registro
     * @param nombreOficina Nombre de la oficina
     */
    void insertarIngresosFP(List<IngresoDto> ingresos, String periodo, String codigoOficina, String nombreOficina);
    
    
    /**
     * Inserta los ingresos en la tabla CON_INGRESOS_TR
     * @param ingresos Ingresos obtenidos como lista
     * @param periodo Periodo definido en formato MM-YYYY
     * @param codigoOficina Codigo de la oficina de registro
     * @param nombreOficina Nombre de la oficina
     */
    void insertarIngresosTR(List<IngresoDto> ingresos, String periodo, String codigoOficina, String nombreOficina);
    
    
    /**
     * Inserta los ingresos en la tabla CON_RESUMEN_CONSOLIDADO
     * @param ingresos Ingresos obtenidos como lista
     * @param periodo Periodo definido en formato MM-YYYY
     * @param codigoOficina Codigo de la oficina de registro
     * @param nombreOficina Nombre de la oficina
     */
    void insertarResumenConsolidado(List<IngresoDto> ingresos, String periodo, String codigoOficina, String nombreOficina);


    /**
     * Obtiene los datos de las fotocopias generadas en la oficina
     * @param codigoOficina Oficina a extraer
     * @param fecha Fecha a obtener
     * @return Lista de recaudo obtenido de la consulta
     */
    List<IngresoReporteDto> obtenerFotocopias(String codigoOficina, LocalDate fecha);
    
    
    /**
     * Obtiene los datos del recaudo de los anulados
     * @param codigoOficina Oficina a extraer
     * @param fecha Fecha a obtener
     * @return Lista de recaudo obtenido de la consulta
     */
    List<IngresoReporteDto> obtenerAnulados(String codigoOficina, LocalDate fecha);
    

    /**
     * Obtiene los datos del restante recaudado por otros conceptos
     * @param codigoOficina Oficina a extraer
     * @param fecha Fecha a obtener
     * @return Lista de recaudo obtenido de la consulta
     */
    List<IngresoReporteDto> obtenerOtrosConceptos(String codigoOficina, LocalDate fecha);
    
    
    /**
     * Obtiene el total de recaudo para una oficina y una fecha
     * @param codigoOficina Oficina a extraer
     * @param fecha Fecha a obtener
     * @return Lista de recaudo obtenido de la consulta
     */
    List<IngresoReporteDto> obtenerConsolidadoIncremento(String codigoOficina, LocalDate fecha);
    
    
    /**
     * Obtiene los datos del recaudo de los exentos
     * @param codigoOficina Oficina a extraer
     * @param fecha Fecha a obtener
     * @return Lista de recaudo obtenido de la consulta
     */
    List<IngresoReporteDto> obtenerExentos(String codigoOficina, LocalDate fecha);
    
    
    /**
     * Obtiene los datos del recaudo de los detallados
     * @param codigoOficina Oficina a extraer
     * @param fecha Fecha a obtener
     * @return Lista de recaudo obtenido de la consulta
     */
    List<IngresoReporteDto> obtenerIngresosFP(String codigoOficina, LocalDate fecha);
    

    /**
     * Obtiene los datos de las formas de pago realizadas con su detalle
     * @param codigoOficina Oficina a extraer
     * @param fecha Fecha a obtener
     * @return Lista de recaudo obtenido de la consulta
     */
    List<IngresoReporteDto> obtenerIngresosTR(String codigoOficina, LocalDate fecha);
    
    
    /**
     * Obtiene los datos de las formas de pago realizadas con su detalle
     * @param codigoOficina Oficina a extraer
     * @param fecha Fecha a obtener
     * @return Lista de recaudo obtenido de la consulta
     */
    List<IngresoReporteDto> obtenerResumenConsolidado(String codigoOficina, LocalDate fecha);

}
