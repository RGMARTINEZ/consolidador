package model.database.oracle.transformer;

import model.dto.IngresoDto;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ResultSetTransformerConsoIncremento {

    public static List<IngresoDto> transformar(ResultSet rs) throws SQLException {
        List<IngresoDto> ingresos = new ArrayList<>();
        IngresoDto ingreso;
        while (rs.next()) {
            ingreso = new IngresoDto();
            int i = 0;
            ingreso.setCirculoOrip(rs.getString(++i));
            ingreso.setFechaTurno(rs.getString(++i));
            ingreso.setValorLiquidacion(rs.getLong(++i));
            ingreso.setValorRecaudoInc(rs.getLong(++i));
            ingreso.setCanalPago(rs.getString(++i));
            ingreso.setFormaPago(rs.getString(++i));
            ingreso.setFechaSistema(rs.getString(++i));
            ingresos.add(ingreso);
        }
        return ingresos;
    }
}

