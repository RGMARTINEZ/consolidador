package model.dto;

public class IngresoDto {

    private String fechaTurno;
    private String turno;
    private String liquidador;
    private String concepto;
    private Long valorBase;
    private Long valorDerechos;
    private Long valorIncremento;
    private String fechaPago;
    private String formaPago;
    private String canalPago;
    private String fechaSistema;
    private String chequeConsig;
    private String pinDocPago;
    private Long valorLiquidacion;
    private Long valorRecaudoInc;
    private String circuloOrip;
    private String tipoLiq;
    
    private String oficina;
    private String fecIni;
    private String fecFin;
    private String tablaBase;
    private Long cantCertif;
    private Long valorCertif;
    private Long cantFotoc;
    private Long valorFotoc;
    
    
    private Long cant_doctos;
    private Long valor_doctos;
    
    private Long cant_mayvr;
    private Long vlr_mayvr;
    
    private Long cant_mayvrc;
    private Long vlr_mayvrc;
    
    private Long cant_otrre;
    private Long vlr_otrre;
    
    private Long cant_docto_anul;
    private Long vlr_docto_anul;
    
    private Long cant_certi_anul;
    private Long vlr_certi_anul;
    
    
    private Long wcantinccdd;
    private Long wvlrinccdd;
    
    private Long wcantinccdmp;
    private Long wvlrinccdmp;






    
    





    public String getFechaTurno() {
        return fechaTurno;
    }

    public void setFechaTurno(String fechaTurno) {
        this.fechaTurno = fechaTurno;
    }

    public String getTurno() {
        return turno;
    }

    public void setTurno(String turno) {
        this.turno = turno;
    }

    public String getLiquidador() {
        return liquidador;
    }

    public void setLiquidador(String liquidador) {
        this.liquidador = liquidador;
    }

    public String getConcepto() {
        return concepto;
    }

    public void setConcepto(String concepto) {
        this.concepto = concepto;
    }

    public Long getValorBase() {
        return valorBase;
    }

    public void setValorBase(Long valorBase) {
        this.valorBase = valorBase;
    }

    public Long getValorDerechos() {
        return valorDerechos;
    }

    public void setValorDerechos(Long valorDerechos) {
        this.valorDerechos = valorDerechos;
    }

    public Long getValorIncremento() {
        return valorIncremento;
    }

    public void setValorIncremento(Long valorIncremento) {
        this.valorIncremento = valorIncremento;
    }

    public String getFechaPago() {
        return fechaPago;
    }

    public void setFechaPago(String fechaPago) {
        this.fechaPago = fechaPago;
    }

    public String getFormaPago() {
        return formaPago;
    }

    public void setFormaPago(final String formaPago) {
        this.formaPago = formaPago;
    }
    

    public String getCanalPago() {
        return canalPago;
    }

    public void setCanalPago(String canalPago) {
        this.canalPago = canalPago;
    }
  

    public String getFechaSistema() {
        return fechaSistema;
    }

    public void setFechaSistema(final String fechaSistema) {
        this.fechaSistema = fechaSistema;
    }
    
    public String getChequeConsig() {
        return chequeConsig;
    }

    public void setChequeConsig(String chequeConsig) {
        this.chequeConsig = chequeConsig;
    }
    
    public String getPinDocPago() {
        return pinDocPago;
    }

    public void setPinDocPago(String pinDocPago) {
        this.pinDocPago = pinDocPago;
    }
    
    public Long getValorLiquidacion() {
        return valorLiquidacion;
    }

    public void setValorLiquidacion(Long valorLiquidacion) {
        this.valorLiquidacion = valorLiquidacion;
    }
    
    public Long getValorRecaudoInc() {
        return valorRecaudoInc;
    }

    public void setValorRecaudoInc(Long valorRecaudoInc) {
        this.valorRecaudoInc = valorRecaudoInc;
    }
    
    public String getCirculoOrip() {
        return circuloOrip;
    }

    public void setCirculoOrip(String circuloOrip) {
        this.circuloOrip = circuloOrip;
    }
    
    public String getTipoLiq() {
        return tipoLiq;
    }

    public void setTipoLiq(String tipoLiq) {
        this.tipoLiq = tipoLiq;
    }
    
    public String getOficina() {
        return oficina;
    }

    public void setOficina(String oficina) {
        this.oficina = oficina;
    }
    
    public String getFecIni() {
        return fecIni;
    }

    public void setFecIni(String fecIni) {
        this.fecIni = fecIni;
    }
    
    public String getFecFin() {
        return fecFin;
    }

    public void setFecFin(String fecFin) {
        this.fecFin = fecFin;
    }
    
    public String getTablaBase() {
        return tablaBase;
    }

    public void setTablaBase(String tablaBase) {
        this.tablaBase = tablaBase;
    }
    
    public Long getCantCertif() {
        return cantCertif;
    }

    public void setCantCertif(Long cantCertif) {
        this.cantCertif = cantCertif;
    }
    
    public Long getValorCertif() {
        return valorCertif;
    }

    public void setValorCertif(Long valorCertif) {
        this.valorCertif = valorCertif;
    }
    
    
    public Long getCantFotoc() {
        return cantFotoc;
    }

    public void setCantFotoc(Long cantFotoc) {
        this.cantFotoc = cantFotoc;
    }
    
    public Long getValorFotoc() {
        return valorFotoc;
    }

    public void setValorFotoc(Long valorFotoc) {
        this.valorFotoc = valorFotoc;
    }
    
    
    
    
    
    
    
    public Long getCantDoctos() {
        return cant_doctos;
    }

    public void setCantDoctos(Long cant_doctos) {
        this.cant_doctos = cant_doctos;
    }
    
    public Long getValorDoctos() {
        return valor_doctos;
    }

    public void setValorDoctos(Long valor_doctos) {
        this.valor_doctos = valor_doctos;
    }
    
    
    
    public Long getCantMayvr() {
        return cant_mayvr;
    }

    public void setCantMayvr(Long cant_mayvr) {
        this.cant_mayvr = cant_mayvr;
    }
    
    public Long getValorMayvr() {
        return vlr_mayvr;
    }

    public void setValorMayvr(Long vlr_mayvr) {
        this.vlr_mayvr = vlr_mayvr;
    }

    
    
    public Long getCantMayvrc() {
        return cant_mayvrc;
    }

    public void setCantMayvrc(Long cant_mayvrc) {
        this.cant_mayvrc = cant_mayvrc;
    }
    
    public Long getValorMayvrc() {
        return vlr_mayvrc;
    }

    public void setValorMayvrc(Long vlr_mayvrc) {
        this.vlr_mayvrc = vlr_mayvrc;
    }
    
    
    public Long getCantOtrre() {
        return cant_otrre;
    }

    public void setCantOtrre(Long cant_otrre) {
        this.cant_otrre = cant_otrre;
    }
    
    public Long getValorOtrre() {
        return vlr_otrre;
    }

    public void setValorOtrre(Long vlr_otrre) {
        this.vlr_otrre = vlr_otrre;
    }
    
    
    
    public Long getCantDoctoAnual() {
        return cant_docto_anul;
    }

    public void setCantDoctoAnual(Long cant_docto_anul) {
        this.cant_docto_anul = cant_docto_anul;
    }
    
    public Long getValorDoctoAnual() {
        return vlr_docto_anul;
    }

    public void setValorDoctoAnual(Long vlr_docto_anul) {
        this.vlr_docto_anul = vlr_docto_anul;
    }
    
    
    public Long getCantCertiAnual() {
        return cant_certi_anul;
    }

    public void setCantCertiAnual(Long cant_certi_anul) {
        this.cant_certi_anul = cant_certi_anul;
    }
    
    public Long getValorCertiAnual() {
        return vlr_certi_anul;
    }

    public void setValorCertiAnual(Long vlr_certi_anul) {
        this.vlr_certi_anul = vlr_certi_anul;
    }
    
    
    public Long getValorWcantinccdd() {
        return wcantinccdd;
    }

    public void setValorWcantinccdd(Long wcantinccdd) {
        this.wcantinccdd = wcantinccdd;
    }
    
    public Long getValorWvlrinccdd() {
        return wvlrinccdd;
    }

    public void setValorWvlrinccdd(Long wvlrinccdd) {
        this.wvlrinccdd = wvlrinccdd;
    }
    
    
    public Long getValorWcantinccdmp() {
        return wcantinccdmp;
    }

    public void setValorWcantinccdmp(Long wcantinccdmp) {
        this.wcantinccdmp = wcantinccdmp;
    }
    
    
    public Long getValorWvlrinccdmp() {
        return wvlrinccdmp;
    }

    public void setValorWvlrinccdmp(Long wvlrinccdmp) {
        this.wvlrinccdmp = wvlrinccdmp;
    }


}
