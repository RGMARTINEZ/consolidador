package model.database.sqlite;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionLite {

    /**
     * Connect to a sample database
     */
    public static Connection connect() {
        Connection conn = null;
        try {
            // db parameters
            // create a connection to the database
            conn = DriverManager.getConnection(DatabaseCreator.URL);
            System.out.println("Conexión establecida SQLite");
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
        if(conn == null){
            DatabaseCreator dc = new DatabaseCreator();
            dc.crear();
        }
        return conn;
    }

}
