package model.database.oracle.transformer;

import model.dto.IngresoDto;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ResultSetTransformer {

    public static List<IngresoDto> transformar(ResultSet rs) throws SQLException {
        List<IngresoDto> ingresos = new ArrayList<>();
        IngresoDto ingreso;
        while (rs.next()) {
            ingreso = new IngresoDto();
            int i = 0;
            ingreso.setFechaTurno(rs.getString(++i));
            ingreso.setTurno(rs.getString(++i));
            ingreso.setLiquidador(rs.getString(++i));
            ingreso.setConcepto(rs.getString(++i));
            ingreso.setValorBase(rs.getLong(++i));
            ingreso.setValorDerechos(rs.getLong(++i));
            ingreso.setValorIncremento(rs.getLong(++i));
            ingreso.setFechaPago(rs.getString(++i));
            ingreso.setFormaPago(rs.getString(++i));
            ingreso.setCanalPago(rs.getString(++i));
            ingreso.setFechaSistema(rs.getString(++i));
            ingreso.setChequeConsig(rs.getString(++i));
            ingreso.setPinDocPago(rs.getString(++i));
            ingreso.setCirculoOrip(rs.getString(++i));
            ingreso.setValorLiquidacion(rs.getLong(++i));
            ingreso.setValorRecaudoInc(rs.getLong(++i));
            ingresos.add(ingreso);
        }
        return ingresos;
    }
    
    
    public static List<IngresoDto> transformarOtrosConceptos(ResultSet rs) throws SQLException {
        List<IngresoDto> ingresos = new ArrayList<>();
        IngresoDto ingreso;
        while (rs.next()) {
            ingreso = new IngresoDto();
            int i = 0;
            ingreso.setFechaTurno(rs.getString(++i));
            ingreso.setTurno(rs.getString(++i));
            ingreso.setLiquidador(rs.getString(++i));
            ingreso.setConcepto(rs.getString(++i));
            ingreso.setValorBase(rs.getLong(++i));
            ingreso.setValorDerechos(rs.getLong(++i));
            ingreso.setValorIncremento(rs.getLong(++i));
            ingreso.setFechaPago(rs.getString(++i));
            ingreso.setFormaPago(rs.getString(++i));
            ingreso.setCanalPago(rs.getString(++i));
            ingreso.setFechaSistema(rs.getString(++i));
            ingreso.setChequeConsig(rs.getString(++i));
            ingreso.setPinDocPago(rs.getString(++i));
            ingresos.add(ingreso);
        }
        return ingresos;
    }
    
    
    public static List<IngresoDto> transformarFotocopias(ResultSet rs) throws SQLException {
        List<IngresoDto> ingresos = new ArrayList<>();
        IngresoDto ingreso;
        while (rs.next()) {
            ingreso = new IngresoDto();
            int i = 0;
            ingreso.setFechaTurno(rs.getString(++i));
            ingreso.setTurno(rs.getString(++i));
            ingreso.setLiquidador(rs.getString(++i));
            ingreso.setConcepto(rs.getString(++i));
            ingreso.setValorBase(rs.getLong(++i));
            ingreso.setValorDerechos(rs.getLong(++i));
            ingreso.setValorIncremento(rs.getLong(++i));
            ingreso.setFechaPago(rs.getString(++i));
            ingreso.setFormaPago(rs.getString(++i));
            ingreso.setCanalPago(rs.getString(++i));
            ingreso.setFechaSistema(rs.getString(++i));
            ingreso.setChequeConsig(rs.getString(++i));
            ingreso.setPinDocPago(rs.getString(++i));
            ingresos.add(ingreso);
        }
        return ingresos;
    }
    
    public static List<IngresoDto> transformarAnulados(ResultSet rs) throws SQLException {
        List<IngresoDto> ingresos = new ArrayList<>();
        IngresoDto ingreso;
        while (rs.next()) {
            ingreso = new IngresoDto();
            int i = 0;
            ingreso.setFechaTurno(rs.getString(++i));
            ingreso.setTurno(rs.getString(++i));
            ingreso.setLiquidador(rs.getString(++i));
            ingreso.setConcepto(rs.getString(++i));
            ingreso.setValorBase(rs.getLong(++i));
            ingreso.setCanalPago(rs.getString(++i));
            ingreso.setFormaPago(rs.getString(++i));
            ingreso.setFechaPago(rs.getString(++i));
            ingreso.setChequeConsig(rs.getString(++i));
            ingreso.setPinDocPago(rs.getString(++i));
            ingresos.add(ingreso);
        }
        return ingresos;
    }
    
    public static List<IngresoDto> transformarIngresosFP(ResultSet rs) throws SQLException {
        List<IngresoDto> ingresos = new ArrayList<>();
        IngresoDto ingreso;
        while (rs.next()) {
            ingreso = new IngresoDto();
            int i = 0;
            ingreso.setFechaTurno(rs.getString(++i));
            ingreso.setTurno(rs.getString(++i));
            ingreso.setLiquidador(rs.getString(++i));
            ingreso.setConcepto(rs.getString(++i));
            ingreso.setValorBase(rs.getLong(++i));
            ingreso.setCanalPago(rs.getString(++i));
            ingreso.setFormaPago(rs.getString(++i));
            ingreso.setFechaPago(rs.getString(++i));
            ingreso.setChequeConsig(rs.getString(++i));
            ingreso.setPinDocPago(rs.getString(++i));
            ingresos.add(ingreso);
        }
        return ingresos;
    }
    
    public static List<IngresoDto> transformarIngresosTR(ResultSet rs) throws SQLException {
        List<IngresoDto> ingresos = new ArrayList<>();
        IngresoDto ingreso;
        while (rs.next()) {
            ingreso = new IngresoDto();
            int i = 0;
            ingreso.setFechaTurno(rs.getString(++i));
            ingreso.setTurno(rs.getString(++i));
            ingreso.setLiquidador(rs.getString(++i));
            ingreso.setConcepto(rs.getString(++i));
            ingreso.setValorBase(rs.getLong(++i));
            ingreso.setValorDerechos(rs.getLong(++i));
            ingreso.setValorIncremento(rs.getLong(++i));
            ingreso.setFechaSistema(rs.getString(++i));
            ingreso.setFechaPago(rs.getString(++i));


            ingresos.add(ingreso);
        }
        return ingresos;
    }
    
    public static List<IngresoDto> transformarResumen(ResultSet rs) throws SQLException {
        List<IngresoDto> ingresos = new ArrayList<>();
        IngresoDto ingreso;
        while (rs.next()) {
            ingreso = new IngresoDto();
            int i = 0;

            ingreso.setOficina(rs.getString(++i));
            ingreso.setFecIni(rs.getString(++i));
            ingreso.setFecFin(rs.getString(++i));
            ingreso.setTablaBase(rs.getString(++i));
            ingreso.setCantCertif(rs.getLong(++i));
            ingreso.setValorCertif(rs.getLong(++i));
            ingreso.setCantFotoc(rs.getLong(++i));
            ingreso.setValorFotoc(rs.getLong(++i));
            ingreso.setCantDoctos(rs.getLong(++i));
            ingreso.setValorDoctos(rs.getLong(++i));
            ingreso.setCantMayvr(rs.getLong(++i));
            ingreso.setValorMayvr(rs.getLong(++i));
            ingreso.setCantMayvrc(rs.getLong(++i));
            ingreso.setValorMayvrc(rs.getLong(++i));
            ingreso.setCantOtrre(rs.getLong(++i));
            ingreso.setValorOtrre(rs.getLong(++i));
            ingreso.setCantDoctoAnual(rs.getLong(++i));
            ingreso.setValorDoctoAnual(rs.getLong(++i));
            ingreso.setCantCertiAnual(rs.getLong(++i));
            ingreso.setValorCertiAnual(rs.getLong(++i));
            ingreso.setValorWcantinccdd(rs.getLong(++i));
            ingreso.setValorWvlrinccdd(rs.getLong(++i));
            ingreso.setValorWcantinccdmp(rs.getLong(++i));
            ingreso.setValorWvlrinccdmp(rs.getLong(++i));
            ingresos.add(ingreso);
        }
        return ingresos;
    }
    
    public static List<IngresoDto> transformarExentos(ResultSet rs) throws SQLException {
        List<IngresoDto> ingresos = new ArrayList<>();
        IngresoDto ingreso;
        while (rs.next()) {
            ingreso = new IngresoDto();
            int i = 0;
            ingreso.setFechaTurno(rs.getString(++i));
            ingreso.setTurno(rs.getString(++i));
            ingreso.setLiquidador(rs.getString(++i));
            ingreso.setConcepto(rs.getString(++i));
            ingreso.setTipoLiq(rs.getString(++i));
            ingreso.setValorDerechos(rs.getLong(++i));
            ingreso.setValorIncremento(rs.getLong(++i));
            ingreso.setFechaSistema(rs.getString(++i));


            ingresos.add(ingreso);
        }
        return ingresos;
    }
    

}

