/*
 * Copyright (c) 2020. Código propiedad de nValue - Realtech.
 * Todos los derechos sobre esté código parcial o totalmente.
 * Si tiene algúna sugerencia que pueda ayudar a mejorar nuestro código no dude en contactarnos.
 * Made by artael
 */

package model.excel;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.streaming.SXSSFCell;
import org.apache.poi.xssf.streaming.SXSSFRow;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class ExcelGenerator {

    private final SXSSFWorkbook workbook;

    public ExcelGenerator() {
        workbook = new SXSSFWorkbook();
    }

    /**
     * Genera una nueva hoja de calculo con la informacion y los headers dados
     * @param informacion Lista de filas donde cada fila contiene un Object[]
     * @param colHeaders Nombres de las columnas
     * @throws Exception Error al generar el reporte solicitado
     */
    public void generate(List<Object> informacion, List<String> colHeaders, String sheetName) throws Exception {
        SXSSFSheet sheet;
        sheet = workbook.createSheet(sheetName);
        sheet.trackAllColumnsForAutoSizing();
        sheet.setZoom(120);
        final int numeroFilas = informacion.size();
        int currentRow = 0;
        SXSSFRow row = sheet.createRow(currentRow);
        row.setHeight((short) 470);
        Object[] valor1;
        if (numeroFilas > 0) {
            valor1 = (Object[]) informacion.get(0);
        } else {
            valor1 = new Object[1];
            valor1[0] = informacion.get(0);
        }
        XSSFCellStyle estiloCabecera = obtenerEstiloCabecera(workbook.getXSSFWorkbook());
        XSSFCellStyle estiloContenido = obtenerEstiloContenido(workbook.getXSSFWorkbook(), IndexedColors.BLACK);
        XSSFCellStyle estiloNumero = obtenerEstiloNumero(workbook.getXSSFWorkbook());
        XSSFCellStyle estiloNegativo = obtenerEstiloNegativo(workbook.getXSSFWorkbook());
        //Crea los headers de las columnas
        for (int i = 0; i < valor1.length; i++) {
            String title = colHeaders.get(i);
            title = title.toUpperCase();
            writeCell(row, i, title, FormatType.TEXT, estiloCabecera);
        }
        currentRow++;
        FormatType tipo;
        //Crea el contenido del excel
        List<Class> clases = new ArrayList<>();
        String valor;
        for (Object objeto : informacion) {
            row = sheet.createRow(currentRow++);
            Object[] cols;
            if (objeto.getClass().isArray()) {
                cols = (Object[]) objeto;
            } else {
                cols = new Object[1];
                cols[0] = objeto;
            }
            for (int i = 0; i < valor1.length; i++) {
                Object value = cols[i];
                if (value == null) {
                    value = "";
                }
                Class _class;
                CellStyle estilo = estiloContenido;
                String strVal = value.toString();
                if (StringUtils.isNumeric(strVal)) {
                    if (strVal.contains("-")) {
                        estilo = estiloNegativo;
                    } else {
                        estilo = estiloNumero;
                    }
                    if (strVal.startsWith("0")) {
                        _class = String.class;
                        value = strVal;
                    } else {
                        if (strVal.length() < 7) {
                            value = Integer.parseInt(strVal);
                            _class = Integer.class;
                        } else {
                            value = new BigInteger(strVal);
                            _class = BigInteger.class;
                        }
                    }
                } else {
                    _class = String.class;
                    value = strVal;
                }
                tipo = getFormatType(_class);
                //logger.info("Escribiendo dato: " + value + ", tipo: " + tipo);
                writeCell(row, i, value, tipo, estilo);
            }
        }
        SXSSFCell cellEmpty;
        row = sheet.createRow(currentRow++);
        row.setHeight((short) 380);
        cellEmpty = row.createCell(colHeaders.size());
        cellEmpty.setCellValue("");
        SXSSFCell cellResumeTitle;
        SXSSFCell cellResume;
        int z = 0;
        for (Class clase : clases) {
            if (clase.equals(Integer.class) && colHeaders.get(z).contains("$")) {
                row = sheet.createRow(currentRow++);
                cellResumeTitle = row.createCell(colHeaders.size() - 2);
                cellResumeTitle.setCellValue(colHeaders.get(z));
                cellResumeTitle.setCellStyle(estiloCabecera);
                cellResume = row.createCell(colHeaders.size() - 1);
                String strFormula = "SUM(" + toAlphabetic(z) + "2:" + toAlphabetic(z) + (numeroFilas + 1) + ")";
                cellResume.setCellType(CellType.FORMULA);
                cellResume.setCellFormula(strFormula);
                cellResume.setCellStyle(estiloContenido);
            }
            z++;
        }
        for (int i = 0; i < colHeaders.size(); i++) {
            try {
                sheet.autoSizeColumn(i);
            } catch (Exception e) {
//                    logger.warn("No es posible autoajustar el tamaño de la columna: " + i, e);
            }
        }
    }

    /**
     * Realiza el almacenamiento del reporte completo
     * @param archivoGuardar File donde se almacenara el archivo
     */
    public void guardarReporte(File archivoGuardar) {
        try (OutputStream os = new FileOutputStream(archivoGuardar)) {
            workbook.write(os);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Obtiene el estilo básico para la cabecera de los exceles.
     *
     * @param wb Workbook relacionado
     * @return Estilo configurado y listo para ser usado
     */
    private static XSSFCellStyle obtenerEstiloCabecera(XSSFWorkbook wb) {
        XSSFFont headerFont = wb.createFont();
        headerFont.setFontHeightInPoints((short) 10);
        headerFont.setFontName("Helvetica");
        headerFont.setColor(IndexedColors.BLACK.getIndex());
        headerFont.setBold(true);
        XSSFCellStyle headerStyle;
        headerStyle = wb.createCellStyle();
        headerStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        headerStyle.setFillForegroundColor(new XSSFColor(new java.awt.Color(233, 233, 233)));
        headerStyle.setFont(headerFont);
        headerStyle.setAlignment(HorizontalAlignment.CENTER);
        headerStyle.setVerticalAlignment(VerticalAlignment.CENTER);
        short borderColorHeader = IndexedColors.GREY_50_PERCENT.getIndex();
        ajustarBordes(headerStyle, borderColorHeader);
        return headerStyle;
    }

    private static XSSFCellStyle obtenerEstiloContenido(XSSFWorkbook wb, IndexedColors color) {
        return colocarPropiedadesGenerales(wb, color);
    }

    private static XSSFCellStyle obtenerEstiloNumero(XSSFWorkbook workbook) {
        return colocarPropiedadesGenerales(workbook, IndexedColors.ROYAL_BLUE);
    }

    private static XSSFCellStyle obtenerEstiloNegativo(XSSFWorkbook workbook) {
        return colocarPropiedadesGenerales(workbook, IndexedColors.DARK_RED);
    }

    private static XSSFCellStyle colocarPropiedadesGenerales(XSSFWorkbook wb, IndexedColors color) {
        XSSFColor colorContenido = new XSSFColor(new java.awt.Color(250, 250, 252));
        short borderColor = IndexedColors.GREY_50_PERCENT.getIndex();
        Font numberFont = wb.createFont();
        numberFont.setFontName("Helvetica");
        numberFont.setFontHeightInPoints((short) 9);
        numberFont.setColor(color.getIndex());
        XSSFCellStyle contentStyle = wb.createCellStyle();
        contentStyle.setFont(numberFont);
        contentStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        contentStyle.setFillForegroundColor(colorContenido);
        contentStyle.setAlignment(HorizontalAlignment.RIGHT);
        ajustarBordes(contentStyle, borderColor);
        return contentStyle;
    }

    private static void ajustarBordes(XSSFCellStyle numberStyle, short borderColor) {
        numberStyle.setBottomBorderColor(borderColor);
        numberStyle.setTopBorderColor(borderColor);
        numberStyle.setLeftBorderColor(borderColor);
        numberStyle.setRightBorderColor(borderColor);
        numberStyle.setBorderBottom(BorderStyle.THIN);
        numberStyle.setBorderTop(BorderStyle.THIN);
        numberStyle.setBorderRight(BorderStyle.THIN);
        numberStyle.setBorderLeft(BorderStyle.THIN);
    }

    private FormatType getFormatType(Class _class) {
        if (_class == Integer.class || _class == Long.class) {
            return FormatType.INTEGER;
        } else if (_class == Float.class || _class == Double.class) {
            return FormatType.FLOAT;
        } else if (_class == Timestamp.class || _class == java.sql.Date.class) {
            return FormatType.DATE;
        } else if (_class == BigDecimal.class) {
            return FormatType.BIGDECIMAL;
        } else if (_class == BigInteger.class) {
            return FormatType.BIGINTEGER;
        } else {
            return FormatType.TEXT;
        }
    }

    private String toAlphabetic(int i) {
        if (i < 0) {
            return "-" + toAlphabetic(-i - 1);
        }
        int quot = i / 26;
        int rem = i % 26;
        char letter = (char) ((int) 'A' + rem);
        if (quot == 0) {
            return "" + letter;
        } else {
            return toAlphabetic(quot - 1) + letter;
        }
    }

    private void writeCell(SXSSFRow row, int col, Object value, FormatType formatType, CellStyle style) {
        SXSSFCell cell = row.createCell(col);
        if (value == null || (value.getClass() == String.class && value.toString().isEmpty())) {
            return;
        }
        if (style == null) {
            style = workbook.createCellStyle();
        }
        DataFormat df = workbook.createDataFormat();
        try {
            switch (formatType) {
                case TEXT:
                    cell.setCellValue(value.toString());
                    break;
                case INTEGER:
                    style.setDataFormat(df.getFormat("#,##0"));
                    cell.setCellValue((Integer) value);
                    cell.setCellType(CellType.NUMERIC);
                    break;
                case BIGDECIMAL:
                    style.setDataFormat(df.getFormat("#,##0"));
                    cell.setCellValue(((BigDecimal) value).doubleValue());
                    cell.setCellType(CellType.NUMERIC);
                    break;
                case BIGINTEGER:
                    style.setDataFormat(df.getFormat("#,##0"));
                    cell.setCellValue(((BigInteger) value).doubleValue());
                    cell.setCellType(CellType.NUMERIC);
                    break;
                case FLOAT:
                    cell.setCellValue(((Number) value).doubleValue());
                    style.setDataFormat(df.getFormat("#,##0.00"));
                    break;
                case DATE:
                    cell.setCellValue((Timestamp) value);
                    style.setDataFormat(df.getFormat("m/d/yy"));
                    break;
                case MONEY:
                    cell.setCellValue(((Number) value).intValue());
                    style.setDataFormat(df.getFormat("($#,##0.00);($#,##0.00)"));
                    break;
                case PERCENTAGE:
                    cell.setCellValue(((Number) value).doubleValue());
                    style.setDataFormat(df.getFormat("0.00%"));
            }
        } catch (Exception e) {
//            logger.error("Error al calcular el valor", e);
            System.out.println("Error en el valor : " + value);
            cell.setCellValue(value.toString());
        }
        cell.setCellStyle(style);
    }


    private enum FormatType {
        TEXT,
        INTEGER,
        FLOAT,
        DATE,
        MONEY,
        PERCENTAGE,
        BIGDECIMAL,
        BIGINTEGER
    }

}
