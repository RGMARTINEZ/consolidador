package model.database.sqlite;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DatabaseCreator {

    private static String PATH = "";
    private static String DBNAME = "dbconsolidator.db";
    public static String URL = "jdbc:sqlite:" + PATH + "" + DBNAME;

    public Connection crear(){
        System.out.println("URL = " + URL);
        Connection conexion = null;
        try (Connection conn = DriverManager.getConnection(URL)) {
            if (conn != null) {
                conexion = conn;
                DatabaseMetaData meta = conn.getMetaData();
                System.out.println("The driver name is " + meta.getDriverName());
                System.out.println("Se ha creado la base de datos");
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return conexion;
    }


}
